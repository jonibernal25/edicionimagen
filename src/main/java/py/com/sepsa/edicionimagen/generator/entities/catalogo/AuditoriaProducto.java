/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "auditoria_producto", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AuditoriaProducto.findAll", query = "SELECT a FROM AuditoriaProducto a"),
    @NamedQuery(name = "AuditoriaProducto.findById", query = "SELECT a FROM AuditoriaProducto a WHERE a.id = :id"),
    @NamedQuery(name = "AuditoriaProducto.findByIdProducto", query = "SELECT a FROM AuditoriaProducto a WHERE a.idProducto = :idProducto"),
    @NamedQuery(name = "AuditoriaProducto.findByIdUsuarioAdd", query = "SELECT a FROM AuditoriaProducto a WHERE a.idUsuarioAdd = :idUsuarioAdd"),
    @NamedQuery(name = "AuditoriaProducto.findByIdUsuarioAud", query = "SELECT a FROM AuditoriaProducto a WHERE a.idUsuarioAud = :idUsuarioAud")})
public class AuditoriaProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_producto")
    private int idProducto;
    @Column(name = "id_usuario_add")
    private Integer idUsuarioAdd;
    @Basic(optional = false)
    @Column(name = "id_usuario_aud")
    private int idUsuarioAud;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAuditoria")
    private Collection<DetalleAuditoria> detalleAuditoriaCollection;

    public AuditoriaProducto() {
    }

    public AuditoriaProducto(Integer id) {
        this.id = id;
    }

    public AuditoriaProducto(Integer id, int idProducto, int idUsuarioAud) {
        this.id = id;
        this.idProducto = idProducto;
        this.idUsuarioAud = idUsuarioAud;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdUsuarioAdd() {
        return idUsuarioAdd;
    }

    public void setIdUsuarioAdd(Integer idUsuarioAdd) {
        this.idUsuarioAdd = idUsuarioAdd;
    }

    public int getIdUsuarioAud() {
        return idUsuarioAud;
    }

    public void setIdUsuarioAud(int idUsuarioAud) {
        this.idUsuarioAud = idUsuarioAud;
    }

    @XmlTransient
    public Collection<DetalleAuditoria> getDetalleAuditoriaCollection() {
        return detalleAuditoriaCollection;
    }

    public void setDetalleAuditoriaCollection(Collection<DetalleAuditoria> detalleAuditoriaCollection) {
        this.detalleAuditoriaCollection = detalleAuditoriaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuditoriaProducto)) {
            return false;
        }
        AuditoriaProducto other = (AuditoriaProducto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.AuditoriaProducto[ id=" + id + " ]";
    }
    
}
