/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "caracteristica", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Caracteristica.findAll", query = "SELECT c FROM Caracteristica c"),
    @NamedQuery(name = "Caracteristica.findById", query = "SELECT c FROM Caracteristica c WHERE c.caracteristicaPK.id = :id"),
    @NamedQuery(name = "Caracteristica.findByIdTipoCaracteristica", query = "SELECT c FROM Caracteristica c WHERE c.caracteristicaPK.idTipoCaracteristica = :idTipoCaracteristica"),
    @NamedQuery(name = "Caracteristica.findByCodigo", query = "SELECT c FROM Caracteristica c WHERE c.codigo = :codigo"),
    @NamedQuery(name = "Caracteristica.findByNombre", query = "SELECT c FROM Caracteristica c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Caracteristica.findByDescripcion", query = "SELECT c FROM Caracteristica c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "Caracteristica.findByGeneral", query = "SELECT c FROM Caracteristica c WHERE c.general = :general"),
    @NamedQuery(name = "Caracteristica.findByValorMultiple", query = "SELECT c FROM Caracteristica c WHERE c.valorMultiple = :valorMultiple"),
    @NamedQuery(name = "Caracteristica.findByValorListaNulo", query = "SELECT c FROM Caracteristica c WHERE c.valorListaNulo = :valorListaNulo"),
    @NamedQuery(name = "Caracteristica.findByNroCampoTxt", query = "SELECT c FROM Caracteristica c WHERE c.nroCampoTxt = :nroCampoTxt"),
    @NamedQuery(name = "Caracteristica.findByEstado", query = "SELECT c FROM Caracteristica c WHERE c.estado = :estado")})
public class Caracteristica implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CaracteristicaPK caracteristicaPK;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "general")
    private Character general;
    @Basic(optional = false)
    @Column(name = "valor_multiple")
    private Character valorMultiple;
    @Basic(optional = false)
    @Column(name = "valor_lista_nulo")
    private Character valorListaNulo;
    @Column(name = "nro_campo_txt")
    private Integer nroCampoTxt;
    @Column(name = "estado")
    private Character estado;
    @OneToMany(mappedBy = "caracteristica")
    private Collection<Caracteristica> caracteristicaCollection;
    @JoinColumns({
        @JoinColumn(name = "id_padre", referencedColumnName = "id"),
        @JoinColumn(name = "id_tipo_caracteristica_padre", referencedColumnName = "id_tipo_caracteristica")})
    @ManyToOne
    private Caracteristica caracteristica;
    @JoinColumn(name = "id_grupo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private GrupoCaracteristica idGrupo;
    @JoinColumn(name = "nro_linea", referencedColumnName = "nro_linea")
    @ManyToOne
    private RegistroTxt nroLinea;
    @JoinColumn(name = "id_tipo_caracteristica", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoCaracteristica tipoCaracteristica;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "caracteristica")
    private Collection<ProductoCaracteristica> productoCaracteristicaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "caracteristica")
    private Collection<ListaValores> listaValoresCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "caracteristica")
    private Collection<CaracteristicaPersona> caracteristicaPersonaCollection;
    @OneToMany(mappedBy = "caracteristica")
    private Collection<DetalleAuditoria> detalleAuditoriaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "caracteristica")
    private Collection<ValorDatoLogistico> valorDatoLogisticoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "caracteristica")
    private Collection<HistoricoProductoCaracteristica> historicoProductoCaracteristicaCollection;

    public Caracteristica() {
    }

    public Caracteristica(CaracteristicaPK caracteristicaPK) {
        this.caracteristicaPK = caracteristicaPK;
    }

    public Caracteristica(CaracteristicaPK caracteristicaPK, String codigo, Character general, Character valorMultiple, Character valorListaNulo) {
        this.caracteristicaPK = caracteristicaPK;
        this.codigo = codigo;
        this.general = general;
        this.valorMultiple = valorMultiple;
        this.valorListaNulo = valorListaNulo;
    }

    public Caracteristica(int id, int idTipoCaracteristica) {
        this.caracteristicaPK = new CaracteristicaPK(id, idTipoCaracteristica);
    }

    public CaracteristicaPK getCaracteristicaPK() {
        return caracteristicaPK;
    }

    public void setCaracteristicaPK(CaracteristicaPK caracteristicaPK) {
        this.caracteristicaPK = caracteristicaPK;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getGeneral() {
        return general;
    }

    public void setGeneral(Character general) {
        this.general = general;
    }

    public Character getValorMultiple() {
        return valorMultiple;
    }

    public void setValorMultiple(Character valorMultiple) {
        this.valorMultiple = valorMultiple;
    }

    public Character getValorListaNulo() {
        return valorListaNulo;
    }

    public void setValorListaNulo(Character valorListaNulo) {
        this.valorListaNulo = valorListaNulo;
    }

    public Integer getNroCampoTxt() {
        return nroCampoTxt;
    }

    public void setNroCampoTxt(Integer nroCampoTxt) {
        this.nroCampoTxt = nroCampoTxt;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    @XmlTransient
    public Collection<Caracteristica> getCaracteristicaCollection() {
        return caracteristicaCollection;
    }

    public void setCaracteristicaCollection(Collection<Caracteristica> caracteristicaCollection) {
        this.caracteristicaCollection = caracteristicaCollection;
    }

    public Caracteristica getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(Caracteristica caracteristica) {
        this.caracteristica = caracteristica;
    }

    public GrupoCaracteristica getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(GrupoCaracteristica idGrupo) {
        this.idGrupo = idGrupo;
    }

    public RegistroTxt getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(RegistroTxt nroLinea) {
        this.nroLinea = nroLinea;
    }

    public TipoCaracteristica getTipoCaracteristica() {
        return tipoCaracteristica;
    }

    public void setTipoCaracteristica(TipoCaracteristica tipoCaracteristica) {
        this.tipoCaracteristica = tipoCaracteristica;
    }

    @XmlTransient
    public Collection<ProductoCaracteristica> getProductoCaracteristicaCollection() {
        return productoCaracteristicaCollection;
    }

    public void setProductoCaracteristicaCollection(Collection<ProductoCaracteristica> productoCaracteristicaCollection) {
        this.productoCaracteristicaCollection = productoCaracteristicaCollection;
    }

    @XmlTransient
    public Collection<ListaValores> getListaValoresCollection() {
        return listaValoresCollection;
    }

    public void setListaValoresCollection(Collection<ListaValores> listaValoresCollection) {
        this.listaValoresCollection = listaValoresCollection;
    }

    @XmlTransient
    public Collection<CaracteristicaPersona> getCaracteristicaPersonaCollection() {
        return caracteristicaPersonaCollection;
    }

    public void setCaracteristicaPersonaCollection(Collection<CaracteristicaPersona> caracteristicaPersonaCollection) {
        this.caracteristicaPersonaCollection = caracteristicaPersonaCollection;
    }

    @XmlTransient
    public Collection<DetalleAuditoria> getDetalleAuditoriaCollection() {
        return detalleAuditoriaCollection;
    }

    public void setDetalleAuditoriaCollection(Collection<DetalleAuditoria> detalleAuditoriaCollection) {
        this.detalleAuditoriaCollection = detalleAuditoriaCollection;
    }

    @XmlTransient
    public Collection<ValorDatoLogistico> getValorDatoLogisticoCollection() {
        return valorDatoLogisticoCollection;
    }

    public void setValorDatoLogisticoCollection(Collection<ValorDatoLogistico> valorDatoLogisticoCollection) {
        this.valorDatoLogisticoCollection = valorDatoLogisticoCollection;
    }

    @XmlTransient
    public Collection<HistoricoProductoCaracteristica> getHistoricoProductoCaracteristicaCollection() {
        return historicoProductoCaracteristicaCollection;
    }

    public void setHistoricoProductoCaracteristicaCollection(Collection<HistoricoProductoCaracteristica> historicoProductoCaracteristicaCollection) {
        this.historicoProductoCaracteristicaCollection = historicoProductoCaracteristicaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (caracteristicaPK != null ? caracteristicaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Caracteristica)) {
            return false;
        }
        Caracteristica other = (Caracteristica) object;
        if ((this.caracteristicaPK == null && other.caracteristicaPK != null) || (this.caracteristicaPK != null && !this.caracteristicaPK.equals(other.caracteristicaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Caracteristica[ caracteristicaPK=" + caracteristicaPK + " ]";
    }
    
}
