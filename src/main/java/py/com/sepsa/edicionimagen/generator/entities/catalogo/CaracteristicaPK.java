/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class CaracteristicaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @Column(name = "id_tipo_caracteristica")
    private int idTipoCaracteristica;

    public CaracteristicaPK() {
    }

    public CaracteristicaPK(int id, int idTipoCaracteristica) {
        this.id = id;
        this.idTipoCaracteristica = idTipoCaracteristica;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTipoCaracteristica() {
        return idTipoCaracteristica;
    }

    public void setIdTipoCaracteristica(int idTipoCaracteristica) {
        this.idTipoCaracteristica = idTipoCaracteristica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) idTipoCaracteristica;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CaracteristicaPK)) {
            return false;
        }
        CaracteristicaPK other = (CaracteristicaPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.idTipoCaracteristica != other.idTipoCaracteristica) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.CaracteristicaPK[ id=" + id + ", idTipoCaracteristica=" + idTipoCaracteristica + " ]";
    }
    
}
