/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "caracteristica_persona", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CaracteristicaPersona.findAll", query = "SELECT c FROM CaracteristicaPersona c"),
    @NamedQuery(name = "CaracteristicaPersona.findById", query = "SELECT c FROM CaracteristicaPersona c WHERE c.caracteristicaPersonaPK.id = :id"),
    @NamedQuery(name = "CaracteristicaPersona.findByIdCaracteristica", query = "SELECT c FROM CaracteristicaPersona c WHERE c.caracteristicaPersonaPK.idCaracteristica = :idCaracteristica"),
    @NamedQuery(name = "CaracteristicaPersona.findByIdTipoCaracteristica", query = "SELECT c FROM CaracteristicaPersona c WHERE c.caracteristicaPersonaPK.idTipoCaracteristica = :idTipoCaracteristica"),
    @NamedQuery(name = "CaracteristicaPersona.findByIdComprador", query = "SELECT c FROM CaracteristicaPersona c WHERE c.caracteristicaPersonaPK.idComprador = :idComprador"),
    @NamedQuery(name = "CaracteristicaPersona.findByIdProveedor", query = "SELECT c FROM CaracteristicaPersona c WHERE c.idProveedor = :idProveedor"),
    @NamedQuery(name = "CaracteristicaPersona.findByMandatorio", query = "SELECT c FROM CaracteristicaPersona c WHERE c.mandatorio = :mandatorio")})
public class CaracteristicaPersona implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CaracteristicaPersonaPK caracteristicaPersonaPK;
    @Column(name = "id_proveedor")
    private Integer idProveedor;
    @Basic(optional = false)
    @Column(name = "mandatorio")
    private Character mandatorio;
    @JoinColumns({
        @JoinColumn(name = "id_caracteristica", referencedColumnName = "id", insertable = false, updatable = false),
        @JoinColumn(name = "id_tipo_caracteristica", referencedColumnName = "id_tipo_caracteristica", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Caracteristica caracteristica;

    public CaracteristicaPersona() {
    }

    public CaracteristicaPersona(CaracteristicaPersonaPK caracteristicaPersonaPK) {
        this.caracteristicaPersonaPK = caracteristicaPersonaPK;
    }

    public CaracteristicaPersona(CaracteristicaPersonaPK caracteristicaPersonaPK, Character mandatorio) {
        this.caracteristicaPersonaPK = caracteristicaPersonaPK;
        this.mandatorio = mandatorio;
    }

    public CaracteristicaPersona(int id, int idCaracteristica, int idTipoCaracteristica, int idComprador) {
        this.caracteristicaPersonaPK = new CaracteristicaPersonaPK(id, idCaracteristica, idTipoCaracteristica, idComprador);
    }

    public CaracteristicaPersonaPK getCaracteristicaPersonaPK() {
        return caracteristicaPersonaPK;
    }

    public void setCaracteristicaPersonaPK(CaracteristicaPersonaPK caracteristicaPersonaPK) {
        this.caracteristicaPersonaPK = caracteristicaPersonaPK;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Character getMandatorio() {
        return mandatorio;
    }

    public void setMandatorio(Character mandatorio) {
        this.mandatorio = mandatorio;
    }

    public Caracteristica getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(Caracteristica caracteristica) {
        this.caracteristica = caracteristica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (caracteristicaPersonaPK != null ? caracteristicaPersonaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CaracteristicaPersona)) {
            return false;
        }
        CaracteristicaPersona other = (CaracteristicaPersona) object;
        if ((this.caracteristicaPersonaPK == null && other.caracteristicaPersonaPK != null) || (this.caracteristicaPersonaPK != null && !this.caracteristicaPersonaPK.equals(other.caracteristicaPersonaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.CaracteristicaPersona[ caracteristicaPersonaPK=" + caracteristicaPersonaPK + " ]";
    }
    
}
