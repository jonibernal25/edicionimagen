/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "categoria", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categoria.findAll", query = "SELECT c FROM Categoria c"),
    @NamedQuery(name = "Categoria.findById", query = "SELECT c FROM Categoria c WHERE c.id = :id"),
    @NamedQuery(name = "Categoria.findByIdPersona", query = "SELECT c FROM Categoria c WHERE c.idPersona = :idPersona"),
    @NamedQuery(name = "Categoria.findByNombre", query = "SELECT c FROM Categoria c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Categoria.findByCodigo", query = "SELECT c FROM Categoria c WHERE c.codigo = :codigo"),
    @NamedQuery(name = "Categoria.findByDescripcion", query = "SELECT c FROM Categoria c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "Categoria.findBySeleccionable", query = "SELECT c FROM Categoria c WHERE c.seleccionable = :seleccionable"),
    @NamedQuery(name = "Categoria.findByEstado", query = "SELECT c FROM Categoria c WHERE c.estado = :estado"),
    @NamedQuery(name = "Categoria.findByIdExterno", query = "SELECT c FROM Categoria c WHERE c.idExterno = :idExterno")})
public class Categoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_persona")
    private Integer idPersona;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "seleccionable")
    private Character seleccionable;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "id_externo")
    private String idExterno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCategoria")
    private Collection<ProductoCategoria> productoCategoriaCollection;
    @OneToMany(mappedBy = "idPadre")
    private Collection<Categoria> categoriaCollection;
    @JoinColumn(name = "id_padre", referencedColumnName = "id")
    @ManyToOne
    private Categoria idPadre;
    @OneToMany(mappedBy = "idBase")
    private Collection<Categoria> categoriaCollection1;
    @JoinColumn(name = "id_base", referencedColumnName = "id")
    @ManyToOne
    private Categoria idBase;
    @JoinColumn(name = "id_tipo_categoria", referencedColumnName = "id")
    @ManyToOne
    private TipoCategoria idTipoCategoria;

    public Categoria() {
    }

    public Categoria(Integer id) {
        this.id = id;
    }

    public Categoria(Integer id, String nombre, Character seleccionable, Character estado) {
        this.id = id;
        this.nombre = nombre;
        this.seleccionable = seleccionable;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getSeleccionable() {
        return seleccionable;
    }

    public void setSeleccionable(Character seleccionable) {
        this.seleccionable = seleccionable;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getIdExterno() {
        return idExterno;
    }

    public void setIdExterno(String idExterno) {
        this.idExterno = idExterno;
    }

    @XmlTransient
    public Collection<ProductoCategoria> getProductoCategoriaCollection() {
        return productoCategoriaCollection;
    }

    public void setProductoCategoriaCollection(Collection<ProductoCategoria> productoCategoriaCollection) {
        this.productoCategoriaCollection = productoCategoriaCollection;
    }

    @XmlTransient
    public Collection<Categoria> getCategoriaCollection() {
        return categoriaCollection;
    }

    public void setCategoriaCollection(Collection<Categoria> categoriaCollection) {
        this.categoriaCollection = categoriaCollection;
    }

    public Categoria getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Categoria idPadre) {
        this.idPadre = idPadre;
    }

    @XmlTransient
    public Collection<Categoria> getCategoriaCollection1() {
        return categoriaCollection1;
    }

    public void setCategoriaCollection1(Collection<Categoria> categoriaCollection1) {
        this.categoriaCollection1 = categoriaCollection1;
    }

    public Categoria getIdBase() {
        return idBase;
    }

    public void setIdBase(Categoria idBase) {
        this.idBase = idBase;
    }

    public TipoCategoria getIdTipoCategoria() {
        return idTipoCategoria;
    }

    public void setIdTipoCategoria(TipoCategoria idTipoCategoria) {
        this.idTipoCategoria = idTipoCategoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoria)) {
            return false;
        }
        Categoria other = (Categoria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Categoria[ id=" + id + " ]";
    }
    
}
