/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "cluster", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cluster.findAll", query = "SELECT c FROM Cluster c"),
    @NamedQuery(name = "Cluster.findById", query = "SELECT c FROM Cluster c WHERE c.id = :id"),
    @NamedQuery(name = "Cluster.findByDescripcion", query = "SELECT c FROM Cluster c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "Cluster.findByCodigo", query = "SELECT c FROM Cluster c WHERE c.codigo = :codigo")})
public class Cluster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "codigo")
    private int codigo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cluster")
    private Collection<LocalCluster> localClusterCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cluster")
    private Collection<ProductoCluster> productoClusterCollection;

    public Cluster() {
    }

    public Cluster(Integer id) {
        this.id = id;
    }

    public Cluster(Integer id, int codigo) {
        this.id = id;
        this.codigo = codigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @XmlTransient
    public Collection<LocalCluster> getLocalClusterCollection() {
        return localClusterCollection;
    }

    public void setLocalClusterCollection(Collection<LocalCluster> localClusterCollection) {
        this.localClusterCollection = localClusterCollection;
    }

    @XmlTransient
    public Collection<ProductoCluster> getProductoClusterCollection() {
        return productoClusterCollection;
    }

    public void setProductoClusterCollection(Collection<ProductoCluster> productoClusterCollection) {
        this.productoClusterCollection = productoClusterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cluster)) {
            return false;
        }
        Cluster other = (Cluster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Cluster[ id=" + id + " ]";
    }
    
}
