/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "codigo_pais", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CodigoPais.findAll", query = "SELECT c FROM CodigoPais c"),
    @NamedQuery(name = "CodigoPais.findById", query = "SELECT c FROM CodigoPais c WHERE c.id = :id"),
    @NamedQuery(name = "CodigoPais.findByNombre", query = "SELECT c FROM CodigoPais c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CodigoPais.findByCodigo", query = "SELECT c FROM CodigoPais c WHERE c.codigo = :codigo"),
    @NamedQuery(name = "CodigoPais.findByCodigoAlpha2", query = "SELECT c FROM CodigoPais c WHERE c.codigoAlpha2 = :codigoAlpha2"),
    @NamedQuery(name = "CodigoPais.findByCodigoAlpha3", query = "SELECT c FROM CodigoPais c WHERE c.codigoAlpha3 = :codigoAlpha3")})
public class CodigoPais implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "codigo")
    private int codigo;
    @Basic(optional = false)
    @Column(name = "codigo_alpha_2")
    private String codigoAlpha2;
    @Basic(optional = false)
    @Column(name = "codigo_alpha_3")
    private String codigoAlpha3;
    @OneToMany(mappedBy = "idCodigoPais")
    private Collection<Subscripcion> subscripcionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCodigoPais")
    private Collection<Sincronizacion> sincronizacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCodigoPais")
    private Collection<CalificadorDescripcionArticulo> calificadorDescripcionArticuloCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCodigoPais")
    private Collection<DetallePublicacion> detallePublicacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCodigoPais")
    private Collection<CodigoSubdivision> codigoSubdivisionCollection;

    public CodigoPais() {
    }

    public CodigoPais(Integer id) {
        this.id = id;
    }

    public CodigoPais(Integer id, String nombre, int codigo, String codigoAlpha2, String codigoAlpha3) {
        this.id = id;
        this.nombre = nombre;
        this.codigo = codigo;
        this.codigoAlpha2 = codigoAlpha2;
        this.codigoAlpha3 = codigoAlpha3;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCodigoAlpha2() {
        return codigoAlpha2;
    }

    public void setCodigoAlpha2(String codigoAlpha2) {
        this.codigoAlpha2 = codigoAlpha2;
    }

    public String getCodigoAlpha3() {
        return codigoAlpha3;
    }

    public void setCodigoAlpha3(String codigoAlpha3) {
        this.codigoAlpha3 = codigoAlpha3;
    }

    @XmlTransient
    public Collection<Subscripcion> getSubscripcionCollection() {
        return subscripcionCollection;
    }

    public void setSubscripcionCollection(Collection<Subscripcion> subscripcionCollection) {
        this.subscripcionCollection = subscripcionCollection;
    }

    @XmlTransient
    public Collection<Sincronizacion> getSincronizacionCollection() {
        return sincronizacionCollection;
    }

    public void setSincronizacionCollection(Collection<Sincronizacion> sincronizacionCollection) {
        this.sincronizacionCollection = sincronizacionCollection;
    }

    @XmlTransient
    public Collection<CalificadorDescripcionArticulo> getCalificadorDescripcionArticuloCollection() {
        return calificadorDescripcionArticuloCollection;
    }

    public void setCalificadorDescripcionArticuloCollection(Collection<CalificadorDescripcionArticulo> calificadorDescripcionArticuloCollection) {
        this.calificadorDescripcionArticuloCollection = calificadorDescripcionArticuloCollection;
    }

    @XmlTransient
    public Collection<DetallePublicacion> getDetallePublicacionCollection() {
        return detallePublicacionCollection;
    }

    public void setDetallePublicacionCollection(Collection<DetallePublicacion> detallePublicacionCollection) {
        this.detallePublicacionCollection = detallePublicacionCollection;
    }

    @XmlTransient
    public Collection<CodigoSubdivision> getCodigoSubdivisionCollection() {
        return codigoSubdivisionCollection;
    }

    public void setCodigoSubdivisionCollection(Collection<CodigoSubdivision> codigoSubdivisionCollection) {
        this.codigoSubdivisionCollection = codigoSubdivisionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CodigoPais)) {
            return false;
        }
        CodigoPais other = (CodigoPais) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.CodigoPais[ id=" + id + " ]";
    }
    
}
