/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ContenedorPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_contenedor")
    private int idContenedor;
    @Basic(optional = false)
    @Column(name = "id_contenido")
    private int idContenido;

    public ContenedorPK() {
    }

    public ContenedorPK(int idContenedor, int idContenido) {
        this.idContenedor = idContenedor;
        this.idContenido = idContenido;
    }

    public int getIdContenedor() {
        return idContenedor;
    }

    public void setIdContenedor(int idContenedor) {
        this.idContenedor = idContenedor;
    }

    public int getIdContenido() {
        return idContenido;
    }

    public void setIdContenido(int idContenido) {
        this.idContenido = idContenido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idContenedor;
        hash += (int) idContenido;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContenedorPK)) {
            return false;
        }
        ContenedorPK other = (ContenedorPK) object;
        if (this.idContenedor != other.idContenedor) {
            return false;
        }
        if (this.idContenido != other.idContenido) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ContenedorPK[ idContenedor=" + idContenedor + ", idContenido=" + idContenido + " ]";
    }
    
}
