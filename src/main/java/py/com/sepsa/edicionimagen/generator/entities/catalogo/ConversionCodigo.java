/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "conversion_codigo", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConversionCodigo.findAll", query = "SELECT c FROM ConversionCodigo c"),
    @NamedQuery(name = "ConversionCodigo.findByIdProducto", query = "SELECT c FROM ConversionCodigo c WHERE c.conversionCodigoPK.idProducto = :idProducto"),
    @NamedQuery(name = "ConversionCodigo.findByIdPersona", query = "SELECT c FROM ConversionCodigo c WHERE c.conversionCodigoPK.idPersona = :idPersona"),
    @NamedQuery(name = "ConversionCodigo.findByCodigo", query = "SELECT c FROM ConversionCodigo c WHERE c.codigo = :codigo")})
public class ConversionCodigo implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ConversionCodigoPK conversionCodigoPK;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;

    public ConversionCodigo() {
    }

    public ConversionCodigo(ConversionCodigoPK conversionCodigoPK) {
        this.conversionCodigoPK = conversionCodigoPK;
    }

    public ConversionCodigo(ConversionCodigoPK conversionCodigoPK, String codigo) {
        this.conversionCodigoPK = conversionCodigoPK;
        this.codigo = codigo;
    }

    public ConversionCodigo(int idProducto, int idPersona) {
        this.conversionCodigoPK = new ConversionCodigoPK(idProducto, idPersona);
    }

    public ConversionCodigoPK getConversionCodigoPK() {
        return conversionCodigoPK;
    }

    public void setConversionCodigoPK(ConversionCodigoPK conversionCodigoPK) {
        this.conversionCodigoPK = conversionCodigoPK;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conversionCodigoPK != null ? conversionCodigoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConversionCodigo)) {
            return false;
        }
        ConversionCodigo other = (ConversionCodigo) object;
        if ((this.conversionCodigoPK == null && other.conversionCodigoPK != null) || (this.conversionCodigoPK != null && !this.conversionCodigoPK.equals(other.conversionCodigoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ConversionCodigo[ conversionCodigoPK=" + conversionCodigoPK + " ]";
    }
    
}
