/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ConversionPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_metrica_origen")
    private int idMetricaOrigen;
    @Basic(optional = false)
    @Column(name = "id_metrica_destino")
    private int idMetricaDestino;

    public ConversionPK() {
    }

    public ConversionPK(int idMetricaOrigen, int idMetricaDestino) {
        this.idMetricaOrigen = idMetricaOrigen;
        this.idMetricaDestino = idMetricaDestino;
    }

    public int getIdMetricaOrigen() {
        return idMetricaOrigen;
    }

    public void setIdMetricaOrigen(int idMetricaOrigen) {
        this.idMetricaOrigen = idMetricaOrigen;
    }

    public int getIdMetricaDestino() {
        return idMetricaDestino;
    }

    public void setIdMetricaDestino(int idMetricaDestino) {
        this.idMetricaDestino = idMetricaDestino;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idMetricaOrigen;
        hash += (int) idMetricaDestino;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConversionPK)) {
            return false;
        }
        ConversionPK other = (ConversionPK) object;
        if (this.idMetricaOrigen != other.idMetricaOrigen) {
            return false;
        }
        if (this.idMetricaDestino != other.idMetricaDestino) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ConversionPK[ idMetricaOrigen=" + idMetricaOrigen + ", idMetricaDestino=" + idMetricaDestino + " ]";
    }
    
}
