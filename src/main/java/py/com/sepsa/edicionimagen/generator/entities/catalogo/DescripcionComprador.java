/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "descripcion_comprador", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DescripcionComprador.findAll", query = "SELECT d FROM DescripcionComprador d"),
    @NamedQuery(name = "DescripcionComprador.findByIdProveedor", query = "SELECT d FROM DescripcionComprador d WHERE d.descripcionCompradorPK.idProveedor = :idProveedor"),
    @NamedQuery(name = "DescripcionComprador.findByIdComprador", query = "SELECT d FROM DescripcionComprador d WHERE d.descripcionCompradorPK.idComprador = :idComprador"),
    @NamedQuery(name = "DescripcionComprador.findByDescripcion", query = "SELECT d FROM DescripcionComprador d WHERE d.descripcion = :descripcion")})
public class DescripcionComprador implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DescripcionCompradorPK descripcionCompradorPK;
    @Column(name = "descripcion")
    private String descripcion;

    public DescripcionComprador() {
    }

    public DescripcionComprador(DescripcionCompradorPK descripcionCompradorPK) {
        this.descripcionCompradorPK = descripcionCompradorPK;
    }

    public DescripcionComprador(int idProveedor, int idComprador) {
        this.descripcionCompradorPK = new DescripcionCompradorPK(idProveedor, idComprador);
    }

    public DescripcionCompradorPK getDescripcionCompradorPK() {
        return descripcionCompradorPK;
    }

    public void setDescripcionCompradorPK(DescripcionCompradorPK descripcionCompradorPK) {
        this.descripcionCompradorPK = descripcionCompradorPK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (descripcionCompradorPK != null ? descripcionCompradorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DescripcionComprador)) {
            return false;
        }
        DescripcionComprador other = (DescripcionComprador) object;
        if ((this.descripcionCompradorPK == null && other.descripcionCompradorPK != null) || (this.descripcionCompradorPK != null && !this.descripcionCompradorPK.equals(other.descripcionCompradorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.DescripcionComprador[ descripcionCompradorPK=" + descripcionCompradorPK + " ]";
    }
    
}
