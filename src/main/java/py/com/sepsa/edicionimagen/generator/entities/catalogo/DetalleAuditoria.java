/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "detalle_auditoria", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleAuditoria.findAll", query = "SELECT d FROM DetalleAuditoria d"),
    @NamedQuery(name = "DetalleAuditoria.findById", query = "SELECT d FROM DetalleAuditoria d WHERE d.id = :id"),
    @NamedQuery(name = "DetalleAuditoria.findByObservacion", query = "SELECT d FROM DetalleAuditoria d WHERE d.observacion = :observacion"),
    @NamedQuery(name = "DetalleAuditoria.findByCorrecto", query = "SELECT d FROM DetalleAuditoria d WHERE d.correcto = :correcto")})
public class DetalleAuditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "observacion")
    private String observacion;
    @Basic(optional = false)
    @Column(name = "correcto")
    private boolean correcto;
    @JoinColumn(name = "id_auditoria", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AuditoriaProducto idAuditoria;
    @JoinColumns({
        @JoinColumn(name = "id_caracteristica", referencedColumnName = "id"),
        @JoinColumn(name = "id_tipo_caracteristica", referencedColumnName = "id_tipo_caracteristica")})
    @ManyToOne
    private Caracteristica caracteristica;

    public DetalleAuditoria() {
    }

    public DetalleAuditoria(Integer id) {
        this.id = id;
    }

    public DetalleAuditoria(Integer id, boolean correcto) {
        this.id = id;
        this.correcto = correcto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(boolean correcto) {
        this.correcto = correcto;
    }

    public AuditoriaProducto getIdAuditoria() {
        return idAuditoria;
    }

    public void setIdAuditoria(AuditoriaProducto idAuditoria) {
        this.idAuditoria = idAuditoria;
    }

    public Caracteristica getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(Caracteristica caracteristica) {
        this.caracteristica = caracteristica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleAuditoria)) {
            return false;
        }
        DetalleAuditoria other = (DetalleAuditoria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.DetalleAuditoria[ id=" + id + " ]";
    }
    
}
