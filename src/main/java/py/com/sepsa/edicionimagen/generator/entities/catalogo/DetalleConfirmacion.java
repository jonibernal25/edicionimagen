/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "detalle_confirmacion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleConfirmacion.findAll", query = "SELECT d FROM DetalleConfirmacion d"),
    @NamedQuery(name = "DetalleConfirmacion.findById", query = "SELECT d FROM DetalleConfirmacion d WHERE d.id = :id"),
    @NamedQuery(name = "DetalleConfirmacion.findByEstado", query = "SELECT d FROM DetalleConfirmacion d WHERE d.estado = :estado")})
public class DetalleConfirmacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;
    @JoinColumn(name = "id_confirmacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Confirmacion idConfirmacion;
    @JoinColumn(name = "id_notificacion", referencedColumnName = "id")
    @ManyToOne
    private Notificacion idNotificacion;
    @JoinColumn(name = "id_segmento_precio", referencedColumnName = "id")
    @ManyToOne
    private SegmentoPrecio idSegmentoPrecio;
    @JoinColumn(name = "id_segmento_relacion", referencedColumnName = "id")
    @ManyToOne
    private SegmentoRelacion idSegmentoRelacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDetalleConfirmacion")
    private Collection<RazonConfirmacion> razonConfirmacionCollection;

    public DetalleConfirmacion() {
    }

    public DetalleConfirmacion(Integer id) {
        this.id = id;
    }

    public DetalleConfirmacion(Integer id, String estado) {
        this.id = id;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Confirmacion getIdConfirmacion() {
        return idConfirmacion;
    }

    public void setIdConfirmacion(Confirmacion idConfirmacion) {
        this.idConfirmacion = idConfirmacion;
    }

    public Notificacion getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(Notificacion idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public SegmentoPrecio getIdSegmentoPrecio() {
        return idSegmentoPrecio;
    }

    public void setIdSegmentoPrecio(SegmentoPrecio idSegmentoPrecio) {
        this.idSegmentoPrecio = idSegmentoPrecio;
    }

    public SegmentoRelacion getIdSegmentoRelacion() {
        return idSegmentoRelacion;
    }

    public void setIdSegmentoRelacion(SegmentoRelacion idSegmentoRelacion) {
        this.idSegmentoRelacion = idSegmentoRelacion;
    }

    @XmlTransient
    public Collection<RazonConfirmacion> getRazonConfirmacionCollection() {
        return razonConfirmacionCollection;
    }

    public void setRazonConfirmacionCollection(Collection<RazonConfirmacion> razonConfirmacionCollection) {
        this.razonConfirmacionCollection = razonConfirmacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleConfirmacion)) {
            return false;
        }
        DetalleConfirmacion other = (DetalleConfirmacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.DetalleConfirmacion[ id=" + id + " ]";
    }
    
}
