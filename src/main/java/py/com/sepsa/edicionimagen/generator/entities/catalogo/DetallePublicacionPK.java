/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class DetallePublicacionPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_publicacion")
    private int idPublicacion;
    @Basic(optional = false)
    @Column(name = "id_tipo_documento")
    private int idTipoDocumento;
    @Basic(optional = false)
    @Column(name = "id_documento")
    private int idDocumento;

    public DetallePublicacionPK() {
    }

    public DetallePublicacionPK(int idPublicacion, int idTipoDocumento, int idDocumento) {
        this.idPublicacion = idPublicacion;
        this.idTipoDocumento = idTipoDocumento;
        this.idDocumento = idDocumento;
    }

    public int getIdPublicacion() {
        return idPublicacion;
    }

    public void setIdPublicacion(int idPublicacion) {
        this.idPublicacion = idPublicacion;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPublicacion;
        hash += (int) idTipoDocumento;
        hash += (int) idDocumento;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetallePublicacionPK)) {
            return false;
        }
        DetallePublicacionPK other = (DetallePublicacionPK) object;
        if (this.idPublicacion != other.idPublicacion) {
            return false;
        }
        if (this.idTipoDocumento != other.idTipoDocumento) {
            return false;
        }
        if (this.idDocumento != other.idDocumento) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.DetallePublicacionPK[ idPublicacion=" + idPublicacion + ", idTipoDocumento=" + idTipoDocumento + ", idDocumento=" + idDocumento + " ]";
    }
    
}
