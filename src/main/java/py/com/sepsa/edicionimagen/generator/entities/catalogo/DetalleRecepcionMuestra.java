/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan
 */
@Entity
@Table(name = "detalle_recepcion_muestra", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
public class DetalleRecepcionMuestra implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetalleRecepcionMuestraPK detalleRecepcionMuestraPK;
    @Basic(optional = false)
    @Column(name = "cantidad")
    private int cantidad;
    @Basic(optional = false)
    @Column(name = "disponible")
    private int disponible;
    @Column(name = "id_devolucion_muestra")
    private Integer idDevolucionMuestra;
    @Column(name = "id_donacion")
    private Integer idDonacion;
    @Basic(optional = false)
    @Column(name = "requiere_documentacion")
    private Character requiereDocumentacion;

    public DetalleRecepcionMuestra() {
    }

    public DetalleRecepcionMuestra(DetalleRecepcionMuestraPK detalleRecepcionMuestraPK) {
        this.detalleRecepcionMuestraPK = detalleRecepcionMuestraPK;
    }

    public DetalleRecepcionMuestra(DetalleRecepcionMuestraPK detalleRecepcionMuestraPK, int cantidad) {
        this.detalleRecepcionMuestraPK = detalleRecepcionMuestraPK;
        this.cantidad = cantidad;
    }

    public DetalleRecepcionMuestra(int idRecepcionMuestra, int idProducto) {
        this.detalleRecepcionMuestraPK = new DetalleRecepcionMuestraPK(idRecepcionMuestra, idProducto);
    }

    public DetalleRecepcionMuestraPK getDetalleRecepcionMuestraPK() {
        return detalleRecepcionMuestraPK;
    }

    public void setDetalleRecepcionMuestraPK(DetalleRecepcionMuestraPK detalleRecepcionMuestraPK) {
        this.detalleRecepcionMuestraPK = detalleRecepcionMuestraPK;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setDisponible(int disponible) {
        this.disponible = disponible;
    }

    public int getDisponible() {
        return disponible;
    }

    public Integer getIdDevolucionMuestra() {
        return idDevolucionMuestra;
    }

    public void setIdDevolucionMuestra(Integer idDevolucionMuestra) {
        this.idDevolucionMuestra = idDevolucionMuestra;
    }

    public Integer getIdDonacion() {
        return idDonacion;
    }

    public void setIdDonacion(Integer idDonacion) {
        this.idDonacion = idDonacion;
    }

    public void setRequiereDocumentacion(Character requiereDocumentacion) {
        this.requiereDocumentacion = requiereDocumentacion;
    }

    public Character getRequiereDocumentacion() {
        return requiereDocumentacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detalleRecepcionMuestraPK != null ? detalleRecepcionMuestraPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleRecepcionMuestra)) {
            return false;
        }
        DetalleRecepcionMuestra other = (DetalleRecepcionMuestra) object;
        if ((this.detalleRecepcionMuestraPK == null && other.detalleRecepcionMuestraPK != null) || (this.detalleRecepcionMuestraPK != null && !this.detalleRecepcionMuestraPK.equals(other.detalleRecepcionMuestraPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.catpy.ejb.entities.catalogo.DetalleRecepcionMuestra[ detalleRecepcionMuestraPK=" + detalleRecepcionMuestraPK + " ]";
    }
    
}
