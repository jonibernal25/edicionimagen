/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan
 */
@Embeddable
public class DetalleRecepcionMuestraPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_recepcion_muestra")
    private int idRecepcionMuestra;
    @Basic(optional = false)
    @Column(name = "id_producto")
    private int idProducto;

    public DetalleRecepcionMuestraPK() {
    }

    public DetalleRecepcionMuestraPK(int idRecepcionMuestra, int idProducto) {
        this.idRecepcionMuestra = idRecepcionMuestra;
        this.idProducto = idProducto;
    }

    public int getIdRecepcionMuestra() {
        return idRecepcionMuestra;
    }

    public void setIdRecepcionMuestra(int idRecepcionMuestra) {
        this.idRecepcionMuestra = idRecepcionMuestra;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idRecepcionMuestra;
        hash += (int) idProducto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleRecepcionMuestraPK)) {
            return false;
        }
        DetalleRecepcionMuestraPK other = (DetalleRecepcionMuestraPK) object;
        if (this.idRecepcionMuestra != other.idRecepcionMuestra) {
            return false;
        }
        if (this.idProducto != other.idProducto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.catpy.ejb.entities.catalogo.DetalleRecepcionMuestraPK[ idRecepcionMuestra=" + idRecepcionMuestra + ", idProducto=" + idProducto + " ]";
    }
    
}
