/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "documento_referencia", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoReferencia.findAll", query = "SELECT d FROM DocumentoReferencia d"),
    @NamedQuery(name = "DocumentoReferencia.findById", query = "SELECT d FROM DocumentoReferencia d WHERE d.id = :id"),
    @NamedQuery(name = "DocumentoReferencia.findByNumero", query = "SELECT d FROM DocumentoReferencia d WHERE d.numero = :numero"),
    @NamedQuery(name = "DocumentoReferencia.findByDescripcion", query = "SELECT d FROM DocumentoReferencia d WHERE d.descripcion = :descripcion")})
public class DocumentoReferencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "numero")
    private String numero;
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "id_segmento_precio", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private SegmentoPrecio idSegmentoPrecio;

    public DocumentoReferencia() {
    }

    public DocumentoReferencia(Integer id) {
        this.id = id;
    }

    public DocumentoReferencia(Integer id, String numero) {
        this.id = id;
        this.numero = numero;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public SegmentoPrecio getIdSegmentoPrecio() {
        return idSegmentoPrecio;
    }

    public void setIdSegmentoPrecio(SegmentoPrecio idSegmentoPrecio) {
        this.idSegmentoPrecio = idSegmentoPrecio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoReferencia)) {
            return false;
        }
        DocumentoReferencia other = (DocumentoReferencia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.DocumentoReferencia[ id=" + id + " ]";
    }
    
}
