/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class EstadoCargaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_producto")
    private int idProducto;
    @Basic(optional = false)
    @Column(name = "id_estado_carga")
    private int idEstadoCarga;

    public EstadoCargaPK() {
    }

    public EstadoCargaPK(int idProducto, int idEstadoCarga) {
        this.idProducto = idProducto;
        this.idEstadoCarga = idEstadoCarga;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdEstadoCarga() {
        return idEstadoCarga;
    }

    public void setIdEstadoCarga(int idEstadoCarga) {
        this.idEstadoCarga = idEstadoCarga;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idProducto;
        hash += (int) idEstadoCarga;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoCargaPK)) {
            return false;
        }
        EstadoCargaPK other = (EstadoCargaPK) object;
        if (this.idProducto != other.idProducto) {
            return false;
        }
        if (this.idEstadoCarga != other.idEstadoCarga) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.EstadoCargaPK[ idProducto=" + idProducto + ", idEstadoCarga=" + idEstadoCarga + " ]";
    }
    
}
