/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "etiqueta", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Etiqueta.findAll", query = "SELECT e FROM Etiqueta e"),
    @NamedQuery(name = "Etiqueta.findById", query = "SELECT e FROM Etiqueta e WHERE e.id = :id"),
    @NamedQuery(name = "Etiqueta.findByDescripcion", query = "SELECT e FROM Etiqueta e WHERE e.descripcion = :descripcion")})
public class Etiqueta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "id_tipo_etiqueta", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoEtiqueta idTipoEtiqueta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "etiqueta")
    private Collection<SegmentoPrecioEtiqueta> segmentoPrecioEtiquetaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "etiqueta")
    private Collection<ProductoEtiqueta> productoEtiquetaCollection;

    public Etiqueta() {
    }

    public Etiqueta(Integer id) {
        this.id = id;
    }

    public Etiqueta(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoEtiqueta getIdTipoEtiqueta() {
        return idTipoEtiqueta;
    }

    public void setIdTipoEtiqueta(TipoEtiqueta idTipoEtiqueta) {
        this.idTipoEtiqueta = idTipoEtiqueta;
    }

    @XmlTransient
    public Collection<SegmentoPrecioEtiqueta> getSegmentoPrecioEtiquetaCollection() {
        return segmentoPrecioEtiquetaCollection;
    }

    public void setSegmentoPrecioEtiquetaCollection(Collection<SegmentoPrecioEtiqueta> segmentoPrecioEtiquetaCollection) {
        this.segmentoPrecioEtiquetaCollection = segmentoPrecioEtiquetaCollection;
    }

    @XmlTransient
    public Collection<ProductoEtiqueta> getProductoEtiquetaCollection() {
        return productoEtiquetaCollection;
    }

    public void setProductoEtiquetaCollection(Collection<ProductoEtiqueta> productoEtiquetaCollection) {
        this.productoEtiquetaCollection = productoEtiquetaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Etiqueta)) {
            return false;
        }
        Etiqueta other = (Etiqueta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Etiqueta[ id=" + id + " ]";
    }
    
}
