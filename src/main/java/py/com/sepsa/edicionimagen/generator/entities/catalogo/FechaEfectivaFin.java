/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "fecha_efectiva_fin", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FechaEfectivaFin.findAll", query = "SELECT f FROM FechaEfectivaFin f"),
    @NamedQuery(name = "FechaEfectivaFin.findById", query = "SELECT f FROM FechaEfectivaFin f WHERE f.id = :id"),
    @NamedQuery(name = "FechaEfectivaFin.findByFin", query = "SELECT f FROM FechaEfectivaFin f WHERE f.fin = :fin"),
    @NamedQuery(name = "FechaEfectivaFin.findByContexto", query = "SELECT f FROM FechaEfectivaFin f WHERE f.contexto = :contexto")})
public class FechaEfectivaFin implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;
    @Basic(optional = false)
    @Column(name = "contexto")
    private String contexto;
    @JoinColumn(name = "id_segmento_precio", referencedColumnName = "id")
    @ManyToOne
    private SegmentoPrecio idSegmentoPrecio;

    public FechaEfectivaFin() {
    }

    public FechaEfectivaFin(Integer id) {
        this.id = id;
    }

    public FechaEfectivaFin(Integer id, Date fin, String contexto) {
        this.id = id;
        this.fin = fin;
        this.contexto = contexto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public String getContexto() {
        return contexto;
    }

    public void setContexto(String contexto) {
        this.contexto = contexto;
    }

    public SegmentoPrecio getIdSegmentoPrecio() {
        return idSegmentoPrecio;
    }

    public void setIdSegmentoPrecio(SegmentoPrecio idSegmentoPrecio) {
        this.idSegmentoPrecio = idSegmentoPrecio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FechaEfectivaFin)) {
            return false;
        }
        FechaEfectivaFin other = (FechaEfectivaFin) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.FechaEfectivaFin[ id=" + id + " ]";
    }
    
}
