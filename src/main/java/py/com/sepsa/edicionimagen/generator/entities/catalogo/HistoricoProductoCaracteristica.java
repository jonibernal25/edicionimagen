/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "historico_producto_caracteristica", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoricoProductoCaracteristica.findAll", query = "SELECT h FROM HistoricoProductoCaracteristica h"),
    @NamedQuery(name = "HistoricoProductoCaracteristica.findById", query = "SELECT h FROM HistoricoProductoCaracteristica h WHERE h.id = :id")})
public class HistoricoProductoCaracteristica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumns({
        @JoinColumn(name = "id_caracteristica", referencedColumnName = "id"),
        @JoinColumn(name = "id_tipo_caracteristica", referencedColumnName = "id_tipo_caracteristica")})
    @ManyToOne(optional = false)
    private Caracteristica caracteristica;
    @JoinColumn(name = "id_publicacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Publicacion idPublicacion;
    @OneToMany(mappedBy = "idHistoricoProductoCaracteristica")
    private Collection<HistoricoValorCaracteristica> historicoValorCaracteristicaCollection;

    public HistoricoProductoCaracteristica() {
    }

    public HistoricoProductoCaracteristica(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Caracteristica getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(Caracteristica caracteristica) {
        this.caracteristica = caracteristica;
    }

    public Publicacion getIdPublicacion() {
        return idPublicacion;
    }

    public void setIdPublicacion(Publicacion idPublicacion) {
        this.idPublicacion = idPublicacion;
    }

    @XmlTransient
    public Collection<HistoricoValorCaracteristica> getHistoricoValorCaracteristicaCollection() {
        return historicoValorCaracteristicaCollection;
    }

    public void setHistoricoValorCaracteristicaCollection(Collection<HistoricoValorCaracteristica> historicoValorCaracteristicaCollection) {
        this.historicoValorCaracteristicaCollection = historicoValorCaracteristicaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoProductoCaracteristica)) {
            return false;
        }
        HistoricoProductoCaracteristica other = (HistoricoProductoCaracteristica) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.HistoricoProductoCaracteristica[ id=" + id + " ]";
    }
    
}
