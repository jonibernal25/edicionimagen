/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "historico_valor_caracteristica", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoricoValorCaracteristica.findAll", query = "SELECT h FROM HistoricoValorCaracteristica h"),
    @NamedQuery(name = "HistoricoValorCaracteristica.findById", query = "SELECT h FROM HistoricoValorCaracteristica h WHERE h.id = :id"),
    @NamedQuery(name = "HistoricoValorCaracteristica.findByValor", query = "SELECT h FROM HistoricoValorCaracteristica h WHERE h.valor = :valor"),
    @NamedQuery(name = "HistoricoValorCaracteristica.findByIdProveedor", query = "SELECT h FROM HistoricoValorCaracteristica h WHERE h.idProveedor = :idProveedor"),
    @NamedQuery(name = "HistoricoValorCaracteristica.findByIdHistoricoValorCaracteristicaPadre", query = "SELECT h FROM HistoricoValorCaracteristica h WHERE h.idHistoricoValorCaracteristicaPadre = :idHistoricoValorCaracteristicaPadre")})
public class HistoricoValorCaracteristica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "valor")
    private String valor;
    @Column(name = "id_proveedor")
    private Integer idProveedor;
    @Column(name = "id_historico_valor_caracteristica_padre")
    private Integer idHistoricoValorCaracteristicaPadre;
    @JoinColumn(name = "id_canal", referencedColumnName = "id")
    @ManyToOne
    private CanalVenta idCanal;
    @JoinColumn(name = "id_historico_producto_caracteristica", referencedColumnName = "id")
    @ManyToOne
    private HistoricoProductoCaracteristica idHistoricoProductoCaracteristica;
    @JoinColumn(name = "id_metrica", referencedColumnName = "id")
    @ManyToOne
    private Metrica idMetrica;

    public HistoricoValorCaracteristica() {
    }

    public HistoricoValorCaracteristica(Integer id) {
        this.id = id;
    }

    public HistoricoValorCaracteristica(Integer id, String valor) {
        this.id = id;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Integer getIdHistoricoValorCaracteristicaPadre() {
        return idHistoricoValorCaracteristicaPadre;
    }

    public void setIdHistoricoValorCaracteristicaPadre(Integer idHistoricoValorCaracteristicaPadre) {
        this.idHistoricoValorCaracteristicaPadre = idHistoricoValorCaracteristicaPadre;
    }

    public CanalVenta getIdCanal() {
        return idCanal;
    }

    public void setIdCanal(CanalVenta idCanal) {
        this.idCanal = idCanal;
    }

    public HistoricoProductoCaracteristica getIdHistoricoProductoCaracteristica() {
        return idHistoricoProductoCaracteristica;
    }

    public void setIdHistoricoProductoCaracteristica(HistoricoProductoCaracteristica idHistoricoProductoCaracteristica) {
        this.idHistoricoProductoCaracteristica = idHistoricoProductoCaracteristica;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoValorCaracteristica)) {
            return false;
        }
        HistoricoValorCaracteristica other = (HistoricoValorCaracteristica) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.HistoricoValorCaracteristica[ id=" + id + " ]";
    }
    
}
