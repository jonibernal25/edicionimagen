/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "lista_valores", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ListaValores.findAll", query = "SELECT l FROM ListaValores l"),
    @NamedQuery(name = "ListaValores.findById", query = "SELECT l FROM ListaValores l WHERE l.listaValoresPK.id = :id"),
    @NamedQuery(name = "ListaValores.findByIdCaracteristica", query = "SELECT l FROM ListaValores l WHERE l.listaValoresPK.idCaracteristica = :idCaracteristica"),
    @NamedQuery(name = "ListaValores.findByIdTipoCaracteristica", query = "SELECT l FROM ListaValores l WHERE l.listaValoresPK.idTipoCaracteristica = :idTipoCaracteristica"),
    @NamedQuery(name = "ListaValores.findByEtiqueta", query = "SELECT l FROM ListaValores l WHERE l.etiqueta = :etiqueta"),
    @NamedQuery(name = "ListaValores.findByValor", query = "SELECT l FROM ListaValores l WHERE l.valor = :valor")})
public class ListaValores implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ListaValoresPK listaValoresPK;
    @Basic(optional = false)
    @Column(name = "etiqueta")
    private String etiqueta;
    @Basic(optional = false)
    @Column(name = "valor")
    private String valor;
    @JoinTable(name = "jerarquia_valores", joinColumns = {
        @JoinColumn(name = "id_hijo", referencedColumnName = "id"),
        @JoinColumn(name = "id_caracteristica_hijo", referencedColumnName = "id_caracteristica"),
        @JoinColumn(name = "id_tipo_caracteristica_hijo", referencedColumnName = "id_tipo_caracteristica")}, inverseJoinColumns = {
        @JoinColumn(name = "id_padre", referencedColumnName = "id"),
        @JoinColumn(name = "id_caracteristica_padre", referencedColumnName = "id_caracteristica"),
        @JoinColumn(name = "id_tipo_caracteristica_padre", referencedColumnName = "id_tipo_caracteristica")})
    @ManyToMany
    private Collection<ListaValores> listaValoresCollection;
    @ManyToMany(mappedBy = "listaValoresCollection")
    private Collection<ListaValores> listaValoresCollection1;
    @JoinColumns({
        @JoinColumn(name = "id_caracteristica", referencedColumnName = "id", insertable = false, updatable = false),
        @JoinColumn(name = "id_tipo_caracteristica", referencedColumnName = "id_tipo_caracteristica", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Caracteristica caracteristica;

    public ListaValores() {
    }

    public ListaValores(ListaValoresPK listaValoresPK) {
        this.listaValoresPK = listaValoresPK;
    }

    public ListaValores(ListaValoresPK listaValoresPK, String etiqueta, String valor) {
        this.listaValoresPK = listaValoresPK;
        this.etiqueta = etiqueta;
        this.valor = valor;
    }

    public ListaValores(int id, int idCaracteristica, int idTipoCaracteristica) {
        this.listaValoresPK = new ListaValoresPK(id, idCaracteristica, idTipoCaracteristica);
    }

    public ListaValoresPK getListaValoresPK() {
        return listaValoresPK;
    }

    public void setListaValoresPK(ListaValoresPK listaValoresPK) {
        this.listaValoresPK = listaValoresPK;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @XmlTransient
    public Collection<ListaValores> getListaValoresCollection() {
        return listaValoresCollection;
    }

    public void setListaValoresCollection(Collection<ListaValores> listaValoresCollection) {
        this.listaValoresCollection = listaValoresCollection;
    }

    @XmlTransient
    public Collection<ListaValores> getListaValoresCollection1() {
        return listaValoresCollection1;
    }

    public void setListaValoresCollection1(Collection<ListaValores> listaValoresCollection1) {
        this.listaValoresCollection1 = listaValoresCollection1;
    }

    public Caracteristica getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(Caracteristica caracteristica) {
        this.caracteristica = caracteristica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (listaValoresPK != null ? listaValoresPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListaValores)) {
            return false;
        }
        ListaValores other = (ListaValores) object;
        if ((this.listaValoresPK == null && other.listaValoresPK != null) || (this.listaValoresPK != null && !this.listaValoresPK.equals(other.listaValoresPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ListaValores[ listaValoresPK=" + listaValoresPK + " ]";
    }
    
}
