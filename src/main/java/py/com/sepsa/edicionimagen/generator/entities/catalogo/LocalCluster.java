/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "local_cluster", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocalCluster.findAll", query = "SELECT l FROM LocalCluster l"),
    @NamedQuery(name = "LocalCluster.findByIdCluster", query = "SELECT l FROM LocalCluster l WHERE l.localClusterPK.idCluster = :idCluster"),
    @NamedQuery(name = "LocalCluster.findByIdPersona", query = "SELECT l FROM LocalCluster l WHERE l.localClusterPK.idPersona = :idPersona"),
    @NamedQuery(name = "LocalCluster.findByIdLocal", query = "SELECT l FROM LocalCluster l WHERE l.localClusterPK.idLocal = :idLocal")})
public class LocalCluster implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LocalClusterPK localClusterPK;
    @JoinColumn(name = "id_cluster", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cluster cluster;

    public LocalCluster() {
    }

    public LocalCluster(LocalClusterPK localClusterPK) {
        this.localClusterPK = localClusterPK;
    }

    public LocalCluster(int idCluster, int idPersona, int idLocal) {
        this.localClusterPK = new LocalClusterPK(idCluster, idPersona, idLocal);
    }

    public LocalClusterPK getLocalClusterPK() {
        return localClusterPK;
    }

    public void setLocalClusterPK(LocalClusterPK localClusterPK) {
        this.localClusterPK = localClusterPK;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (localClusterPK != null ? localClusterPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocalCluster)) {
            return false;
        }
        LocalCluster other = (LocalCluster) object;
        if ((this.localClusterPK == null && other.localClusterPK != null) || (this.localClusterPK != null && !this.localClusterPK.equals(other.localClusterPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.LocalCluster[ localClusterPK=" + localClusterPK + " ]";
    }
    
}
