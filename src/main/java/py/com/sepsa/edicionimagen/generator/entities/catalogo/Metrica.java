/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "metrica", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Metrica.findAll", query = "SELECT m FROM Metrica m"),
    @NamedQuery(name = "Metrica.findById", query = "SELECT m FROM Metrica m WHERE m.id = :id"),
    @NamedQuery(name = "Metrica.findByCodigo", query = "SELECT m FROM Metrica m WHERE m.codigo = :codigo"),
    @NamedQuery(name = "Metrica.findByDescripcion", query = "SELECT m FROM Metrica m WHERE m.descripcion = :descripcion")})
public class Metrica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "descripcion")
    private String descripcion;
    @JoinTable(name = "metrica_tipo_caracteristica", joinColumns = {
        @JoinColumn(name = "id_metrica", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_tipo_caracteristica", referencedColumnName = "id")})
    @ManyToMany
    private Collection<TipoCaracteristica> tipoCaracteristicaCollection;
    @OneToMany(mappedBy = "idMetrica")
    private Collection<ValorCaracteristica> valorCaracteristicaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "metrica")
    private Collection<Conversion> conversionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "metrica1")
    private Collection<Conversion> conversionCollection1;
    @OneToMany(mappedBy = "idMetrica")
    private Collection<ValorDatoLogistico> valorDatoLogisticoCollection;
    @OneToMany(mappedBy = "idMetrica")
    private Collection<HistoricoValorCaracteristica> historicoValorCaracteristicaCollection;

    public Metrica() {
    }

    public Metrica(Integer id) {
        this.id = id;
    }

    public Metrica(Integer id, String codigo) {
        this.id = id;
        this.codigo = codigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<TipoCaracteristica> getTipoCaracteristicaCollection() {
        return tipoCaracteristicaCollection;
    }

    public void setTipoCaracteristicaCollection(Collection<TipoCaracteristica> tipoCaracteristicaCollection) {
        this.tipoCaracteristicaCollection = tipoCaracteristicaCollection;
    }

    @XmlTransient
    public Collection<ValorCaracteristica> getValorCaracteristicaCollection() {
        return valorCaracteristicaCollection;
    }

    public void setValorCaracteristicaCollection(Collection<ValorCaracteristica> valorCaracteristicaCollection) {
        this.valorCaracteristicaCollection = valorCaracteristicaCollection;
    }

    @XmlTransient
    public Collection<Conversion> getConversionCollection() {
        return conversionCollection;
    }

    public void setConversionCollection(Collection<Conversion> conversionCollection) {
        this.conversionCollection = conversionCollection;
    }

    @XmlTransient
    public Collection<Conversion> getConversionCollection1() {
        return conversionCollection1;
    }

    public void setConversionCollection1(Collection<Conversion> conversionCollection1) {
        this.conversionCollection1 = conversionCollection1;
    }

    @XmlTransient
    public Collection<ValorDatoLogistico> getValorDatoLogisticoCollection() {
        return valorDatoLogisticoCollection;
    }

    public void setValorDatoLogisticoCollection(Collection<ValorDatoLogistico> valorDatoLogisticoCollection) {
        this.valorDatoLogisticoCollection = valorDatoLogisticoCollection;
    }

    @XmlTransient
    public Collection<HistoricoValorCaracteristica> getHistoricoValorCaracteristicaCollection() {
        return historicoValorCaracteristicaCollection;
    }

    public void setHistoricoValorCaracteristicaCollection(Collection<HistoricoValorCaracteristica> historicoValorCaracteristicaCollection) {
        this.historicoValorCaracteristicaCollection = historicoValorCaracteristicaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Metrica)) {
            return false;
        }
        Metrica other = (Metrica) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Metrica[ id=" + id + " ]";
    }
    
}
