/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "notificacion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Notificacion.findAll", query = "SELECT n FROM Notificacion n"),
    @NamedQuery(name = "Notificacion.findById", query = "SELECT n FROM Notificacion n WHERE n.id = :id"),
    @NamedQuery(name = "Notificacion.findByEnviado", query = "SELECT n FROM Notificacion n WHERE n.enviado = :enviado"),
    @NamedQuery(name = "Notificacion.findByIdDocumento", query = "SELECT n FROM Notificacion n WHERE n.idDocumento = :idDocumento"),
    @NamedQuery(name = "Notificacion.findByIdTipoDocumento", query = "SELECT n FROM Notificacion n WHERE n.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "Notificacion.findByIdSincronizacion", query = "SELECT n FROM Notificacion n WHERE n.idSincronizacion = :idSincronizacion"),
    @NamedQuery(name = "Notificacion.findByFechaInsercion", query = "SELECT n FROM Notificacion n WHERE n.fechaInsercion = :fechaInsercion")})
public class Notificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "enviado")
    private Character enviado;
    @Column(name = "id_documento")
    private Integer idDocumento;
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;
    @Basic(optional = false)
    @Column(name = "id_sincronizacion")
    private int idSincronizacion;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @OneToMany(mappedBy = "idNotificacion")
    private Collection<DetalleConfirmacion> detalleConfirmacionCollection;
    @OneToMany(mappedBy = "idNotificacion")
    private Collection<SegmentoPrecio> segmentoPrecioCollection;
    @OneToMany(mappedBy = "idNotificacion")
    private Collection<UsuarioMensaje> usuarioMensajeCollection;
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Sincronizacion sincronizacion;

    public Notificacion() {
    }

    public Notificacion(Integer id) {
        this.id = id;
    }

    public Notificacion(Integer id, Character enviado, int idSincronizacion) {
        this.id = id;
        this.enviado = enviado;
        this.idSincronizacion = idSincronizacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Character getEnviado() {
        return enviado;
    }

    public void setEnviado(Character enviado) {
        this.enviado = enviado;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public int getIdSincronizacion() {
        return idSincronizacion;
    }

    public void setIdSincronizacion(int idSincronizacion) {
        this.idSincronizacion = idSincronizacion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    @XmlTransient
    public Collection<DetalleConfirmacion> getDetalleConfirmacionCollection() {
        return detalleConfirmacionCollection;
    }

    public void setDetalleConfirmacionCollection(Collection<DetalleConfirmacion> detalleConfirmacionCollection) {
        this.detalleConfirmacionCollection = detalleConfirmacionCollection;
    }

    @XmlTransient
    public Collection<SegmentoPrecio> getSegmentoPrecioCollection() {
        return segmentoPrecioCollection;
    }

    public void setSegmentoPrecioCollection(Collection<SegmentoPrecio> segmentoPrecioCollection) {
        this.segmentoPrecioCollection = segmentoPrecioCollection;
    }

    @XmlTransient
    public Collection<UsuarioMensaje> getUsuarioMensajeCollection() {
        return usuarioMensajeCollection;
    }

    public void setUsuarioMensajeCollection(Collection<UsuarioMensaje> usuarioMensajeCollection) {
        this.usuarioMensajeCollection = usuarioMensajeCollection;
    }

    public Sincronizacion getSincronizacion() {
        return sincronizacion;
    }

    public void setSincronizacion(Sincronizacion sincronizacion) {
        this.sincronizacion = sincronizacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notificacion)) {
            return false;
        }
        Notificacion other = (Notificacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Notificacion[ id=" + id + " ]";
    }
    
}
