/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "pantalla", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pantalla.findAll", query = "SELECT p FROM Pantalla p"),
    @NamedQuery(name = "Pantalla.findById", query = "SELECT p FROM Pantalla p WHERE p.id = :id"),
    @NamedQuery(name = "Pantalla.findByNombre", query = "SELECT p FROM Pantalla p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Pantalla.findByUrl", query = "SELECT p FROM Pantalla p WHERE p.url = :url"),
    @NamedQuery(name = "Pantalla.findByDescripcion", query = "SELECT p FROM Pantalla p WHERE p.descripcion = :descripcion")})
public class Pantalla implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "url")
    private String url;
    @Column(name = "descripcion")
    private String descripcion;
    @JoinTable(name = "perfil_pantalla", joinColumns = {
        @JoinColumn(name = "id_pantalla", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_perfil", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Perfil> perfilCollection;
    @OneToMany(mappedBy = "idPadre")
    private Collection<Pantalla> pantallaCollection;
    @JoinColumn(name = "id_padre", referencedColumnName = "id")
    @ManyToOne
    private Pantalla idPadre;

    public Pantalla() {
    }

    public Pantalla(Integer id) {
        this.id = id;
    }

    public Pantalla(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<Perfil> getPerfilCollection() {
        return perfilCollection;
    }

    public void setPerfilCollection(Collection<Perfil> perfilCollection) {
        this.perfilCollection = perfilCollection;
    }

    @XmlTransient
    public Collection<Pantalla> getPantallaCollection() {
        return pantallaCollection;
    }

    public void setPantallaCollection(Collection<Pantalla> pantallaCollection) {
        this.pantallaCollection = pantallaCollection;
    }

    public Pantalla getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Pantalla idPadre) {
        this.idPadre = idPadre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pantalla)) {
            return false;
        }
        Pantalla other = (Pantalla) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Pantalla[ id=" + id + " ]";
    }
    
}
