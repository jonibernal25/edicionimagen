/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import py.com.sepsa.edicionimagen.generator.entities.info.Producto;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "producto_caracteristica", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
public class ProductoCaracteristica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    /* @Basic(optional = false)
    @NotNull
    @Column(name = "id_producto")
    private int idProducto;*/
    @JoinColumn(name = "id_producto", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Producto producto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productoCaracteristica")
    private Collection<ValorCaracteristica> valorCaracteristicaCollection;
    @JoinColumns({
        @JoinColumn(name = "id_caracteristica", referencedColumnName = "id"),
        @JoinColumn(name = "id_tipo_caracteristica", referencedColumnName = "id_tipo_caracteristica")})
    @ManyToOne(optional = false)
    private Caracteristica caracteristica;

    public ProductoCaracteristica() {
    }

    public ProductoCaracteristica(Integer id) {
        this.id = id;
    }

    public ProductoCaracteristica(Integer id, int idProducto) {
        this.id = id;
        //this.idProducto = idProducto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    @XmlTransient
    public Collection<ValorCaracteristica> getValorCaracteristicaCollection() {
        return valorCaracteristicaCollection;
    }

    public void setValorCaracteristicaCollection(Collection<ValorCaracteristica> valorCaracteristicaCollection) {
        this.valorCaracteristicaCollection = valorCaracteristicaCollection;
    }

    public Caracteristica getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(Caracteristica caracteristica) {
        this.caracteristica = caracteristica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoCaracteristica)) {
            return false;
        }
        ProductoCaracteristica other = (ProductoCaracteristica) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ProductoCaracteristica[ id=" + id + " ]";
    }
    
}
