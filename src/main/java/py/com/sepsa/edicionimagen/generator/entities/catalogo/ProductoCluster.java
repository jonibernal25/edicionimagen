/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "producto_cluster", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoCluster.findAll", query = "SELECT p FROM ProductoCluster p"),
    @NamedQuery(name = "ProductoCluster.findByIdCluster", query = "SELECT p FROM ProductoCluster p WHERE p.productoClusterPK.idCluster = :idCluster"),
    @NamedQuery(name = "ProductoCluster.findByIdProducto", query = "SELECT p FROM ProductoCluster p WHERE p.productoClusterPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoCluster.findByFechaInsercion", query = "SELECT p FROM ProductoCluster p WHERE p.fechaInsercion = :fechaInsercion"),
    @NamedQuery(name = "ProductoCluster.findByIdUsuario", query = "SELECT p FROM ProductoCluster p WHERE p.idUsuario = :idUsuario"),
    @NamedQuery(name = "ProductoCluster.findByIdPersona", query = "SELECT p FROM ProductoCluster p WHERE p.productoClusterPK.idPersona = :idPersona"),
    @NamedQuery(name = "ProductoCluster.findByIdLocal", query = "SELECT p FROM ProductoCluster p WHERE p.productoClusterPK.idLocal = :idLocal")})
public class ProductoCluster implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoClusterPK productoClusterPK;
    @Basic(optional = false)
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private int idUsuario;
    @JoinColumn(name = "id_cluster", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cluster cluster;

    public ProductoCluster() {
    }

    public ProductoCluster(ProductoClusterPK productoClusterPK) {
        this.productoClusterPK = productoClusterPK;
    }

    public ProductoCluster(ProductoClusterPK productoClusterPK, Date fechaInsercion, int idUsuario) {
        this.productoClusterPK = productoClusterPK;
        this.fechaInsercion = fechaInsercion;
        this.idUsuario = idUsuario;
    }

    public ProductoCluster(int idCluster, int idProducto, int idPersona, int idLocal) {
        this.productoClusterPK = new ProductoClusterPK(idCluster, idProducto, idPersona, idLocal);
    }

    public ProductoClusterPK getProductoClusterPK() {
        return productoClusterPK;
    }

    public void setProductoClusterPK(ProductoClusterPK productoClusterPK) {
        this.productoClusterPK = productoClusterPK;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoClusterPK != null ? productoClusterPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoCluster)) {
            return false;
        }
        ProductoCluster other = (ProductoCluster) object;
        if ((this.productoClusterPK == null && other.productoClusterPK != null) || (this.productoClusterPK != null && !this.productoClusterPK.equals(other.productoClusterPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ProductoCluster[ productoClusterPK=" + productoClusterPK + " ]";
    }
    
}
