/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ProductoEtiquetaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_producto")
    private int idProducto;
    @Basic(optional = false)
    @Column(name = "id_etiqueta")
    private int idEtiqueta;

    public ProductoEtiquetaPK() {
    }

    public ProductoEtiquetaPK(int idProducto, int idEtiqueta) {
        this.idProducto = idProducto;
        this.idEtiqueta = idEtiqueta;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdEtiqueta() {
        return idEtiqueta;
    }

    public void setIdEtiqueta(int idEtiqueta) {
        this.idEtiqueta = idEtiqueta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idProducto;
        hash += (int) idEtiqueta;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoEtiquetaPK)) {
            return false;
        }
        ProductoEtiquetaPK other = (ProductoEtiquetaPK) object;
        if (this.idProducto != other.idProducto) {
            return false;
        }
        if (this.idEtiqueta != other.idEtiqueta) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ProductoEtiquetaPK[ idProducto=" + idProducto + ", idEtiqueta=" + idEtiqueta + " ]";
    }
    
}
