/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "promocion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Promocion.findAll", query = "SELECT p FROM Promocion p"),
    @NamedQuery(name = "Promocion.findById", query = "SELECT p FROM Promocion p WHERE p.id = :id"),
    @NamedQuery(name = "Promocion.findByIdProducto", query = "SELECT p FROM Promocion p WHERE p.idProducto = :idProducto"),
    @NamedQuery(name = "Promocion.findByFechaInicio", query = "SELECT p FROM Promocion p WHERE p.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Promocion.findByFechaFin", query = "SELECT p FROM Promocion p WHERE p.fechaFin = :fechaFin"),
    @NamedQuery(name = "Promocion.findByPrecioDescuento", query = "SELECT p FROM Promocion p WHERE p.precioDescuento = :precioDescuento"),
    @NamedQuery(name = "Promocion.findByPorcentajeDescuento", query = "SELECT p FROM Promocion p WHERE p.porcentajeDescuento = :porcentajeDescuento"),
    @NamedQuery(name = "Promocion.findByCantidadMinima", query = "SELECT p FROM Promocion p WHERE p.cantidadMinima = :cantidadMinima"),
    @NamedQuery(name = "Promocion.findByCantidadPromocion", query = "SELECT p FROM Promocion p WHERE p.cantidadPromocion = :cantidadPromocion"),
    @NamedQuery(name = "Promocion.findByEstado", query = "SELECT p FROM Promocion p WHERE p.estado = :estado")})
public class Promocion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_producto")
    private Integer idProducto;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Column(name = "precio_descuento")
    private BigInteger precioDescuento;
    @Column(name = "porcentaje_descuento")
    private Integer porcentajeDescuento;
    @Column(name = "cantidad_minima")
    private Integer cantidadMinima;
    @Column(name = "cantidad_promocion")
    private Integer cantidadPromocion;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinColumn(name = "id_canal_venta", referencedColumnName = "id")
    @ManyToOne
    private CanalVenta idCanalVenta;
    @JoinColumn(name = "id_tipo_promocion", referencedColumnName = "id")
    @ManyToOne
    private TipoPromocion idTipoPromocion;

    public Promocion() {
    }

    public Promocion(Integer id) {
        this.id = id;
    }

    public Promocion(Integer id, Character estado) {
        this.id = id;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public BigInteger getPrecioDescuento() {
        return precioDescuento;
    }

    public void setPrecioDescuento(BigInteger precioDescuento) {
        this.precioDescuento = precioDescuento;
    }

    public Integer getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(Integer porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public Integer getCantidadMinima() {
        return cantidadMinima;
    }

    public void setCantidadMinima(Integer cantidadMinima) {
        this.cantidadMinima = cantidadMinima;
    }

    public Integer getCantidadPromocion() {
        return cantidadPromocion;
    }

    public void setCantidadPromocion(Integer cantidadPromocion) {
        this.cantidadPromocion = cantidadPromocion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public CanalVenta getIdCanalVenta() {
        return idCanalVenta;
    }

    public void setIdCanalVenta(CanalVenta idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    public TipoPromocion getIdTipoPromocion() {
        return idTipoPromocion;
    }

    public void setIdTipoPromocion(TipoPromocion idTipoPromocion) {
        this.idTipoPromocion = idTipoPromocion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Promocion)) {
            return false;
        }
        Promocion other = (Promocion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Promocion[ id=" + id + " ]";
    }
    
}
