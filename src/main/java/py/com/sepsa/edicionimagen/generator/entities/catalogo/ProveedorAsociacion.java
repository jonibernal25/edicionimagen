/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "proveedor_asociacion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProveedorAsociacion.findAll", query = "SELECT p FROM ProveedorAsociacion p"),
    @NamedQuery(name = "ProveedorAsociacion.findByIdProveedor", query = "SELECT p FROM ProveedorAsociacion p WHERE p.proveedorAsociacionPK.idProveedor = :idProveedor"),
    @NamedQuery(name = "ProveedorAsociacion.findByIdAsociacion", query = "SELECT p FROM ProveedorAsociacion p WHERE p.proveedorAsociacionPK.idAsociacion = :idAsociacion")})
public class ProveedorAsociacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProveedorAsociacionPK proveedorAsociacionPK;

    public ProveedorAsociacion() {
    }

    public ProveedorAsociacion(ProveedorAsociacionPK proveedorAsociacionPK) {
        this.proveedorAsociacionPK = proveedorAsociacionPK;
    }

    public ProveedorAsociacion(int idProveedor, int idAsociacion) {
        this.proveedorAsociacionPK = new ProveedorAsociacionPK(idProveedor, idAsociacion);
    }

    public ProveedorAsociacionPK getProveedorAsociacionPK() {
        return proveedorAsociacionPK;
    }

    public void setProveedorAsociacionPK(ProveedorAsociacionPK proveedorAsociacionPK) {
        this.proveedorAsociacionPK = proveedorAsociacionPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proveedorAsociacionPK != null ? proveedorAsociacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProveedorAsociacion)) {
            return false;
        }
        ProveedorAsociacion other = (ProveedorAsociacion) object;
        if ((this.proveedorAsociacionPK == null && other.proveedorAsociacionPK != null) || (this.proveedorAsociacionPK != null && !this.proveedorAsociacionPK.equals(other.proveedorAsociacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ProveedorAsociacion[ proveedorAsociacionPK=" + proveedorAsociacionPK + " ]";
    }
    
}
