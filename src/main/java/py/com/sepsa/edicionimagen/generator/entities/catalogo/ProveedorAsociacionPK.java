/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ProveedorAsociacionPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_proveedor")
    private int idProveedor;
    @Basic(optional = false)
    @Column(name = "id_asociacion")
    private int idAsociacion;

    public ProveedorAsociacionPK() {
    }

    public ProveedorAsociacionPK(int idProveedor, int idAsociacion) {
        this.idProveedor = idProveedor;
        this.idAsociacion = idAsociacion;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public int getIdAsociacion() {
        return idAsociacion;
    }

    public void setIdAsociacion(int idAsociacion) {
        this.idAsociacion = idAsociacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idProveedor;
        hash += (int) idAsociacion;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProveedorAsociacionPK)) {
            return false;
        }
        ProveedorAsociacionPK other = (ProveedorAsociacionPK) object;
        if (this.idProveedor != other.idProveedor) {
            return false;
        }
        if (this.idAsociacion != other.idAsociacion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ProveedorAsociacionPK[ idProveedor=" + idProveedor + ", idAsociacion=" + idAsociacion + " ]";
    }
    
}
