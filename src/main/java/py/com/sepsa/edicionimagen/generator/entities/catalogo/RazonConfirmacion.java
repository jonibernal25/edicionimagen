/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "razon_confirmacion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RazonConfirmacion.findAll", query = "SELECT r FROM RazonConfirmacion r"),
    @NamedQuery(name = "RazonConfirmacion.findById", query = "SELECT r FROM RazonConfirmacion r WHERE r.id = :id"),
    @NamedQuery(name = "RazonConfirmacion.findByRazon", query = "SELECT r FROM RazonConfirmacion r WHERE r.razon = :razon"),
    @NamedQuery(name = "RazonConfirmacion.findByAccionNecesaria", query = "SELECT r FROM RazonConfirmacion r WHERE r.accionNecesaria = :accionNecesaria"),
    @NamedQuery(name = "RazonConfirmacion.findByDescripcion", query = "SELECT r FROM RazonConfirmacion r WHERE r.descripcion = :descripcion")})
public class RazonConfirmacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "razon")
    private String razon;
    @Column(name = "accion_necesaria")
    private String accionNecesaria;
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "id_detalle_confirmacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DetalleConfirmacion idDetalleConfirmacion;

    public RazonConfirmacion() {
    }

    public RazonConfirmacion(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRazon() {
        return razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public String getAccionNecesaria() {
        return accionNecesaria;
    }

    public void setAccionNecesaria(String accionNecesaria) {
        this.accionNecesaria = accionNecesaria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public DetalleConfirmacion getIdDetalleConfirmacion() {
        return idDetalleConfirmacion;
    }

    public void setIdDetalleConfirmacion(DetalleConfirmacion idDetalleConfirmacion) {
        this.idDetalleConfirmacion = idDetalleConfirmacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RazonConfirmacion)) {
            return false;
        }
        RazonConfirmacion other = (RazonConfirmacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.RazonConfirmacion[ id=" + id + " ]";
    }
    
}
