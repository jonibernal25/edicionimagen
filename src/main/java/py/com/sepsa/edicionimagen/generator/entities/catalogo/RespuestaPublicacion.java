/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "respuesta_publicacion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RespuestaPublicacion.findAll", query = "SELECT r FROM RespuestaPublicacion r"),
    @NamedQuery(name = "RespuestaPublicacion.findById", query = "SELECT r FROM RespuestaPublicacion r WHERE r.respuestaPublicacionPK.id = :id"),
    @NamedQuery(name = "RespuestaPublicacion.findByIdPublicacion", query = "SELECT r FROM RespuestaPublicacion r WHERE r.respuestaPublicacionPK.idPublicacion = :idPublicacion"),
    @NamedQuery(name = "RespuestaPublicacion.findByIdDocumento", query = "SELECT r FROM RespuestaPublicacion r WHERE r.idDocumento = :idDocumento"),
    @NamedQuery(name = "RespuestaPublicacion.findByIdTipoDocumento", query = "SELECT r FROM RespuestaPublicacion r WHERE r.idTipoDocumento = :idTipoDocumento")})
public class RespuestaPublicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RespuestaPublicacionPK respuestaPublicacionPK;
    @Column(name = "id_documento")
    private Integer idDocumento;
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;
    @JoinColumn(name = "id_motivo", referencedColumnName = "id")
    @ManyToOne
    private Motivo idMotivo;
    @JoinColumn(name = "id_publicacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Publicacion publicacion;

    public RespuestaPublicacion() {
    }

    public RespuestaPublicacion(RespuestaPublicacionPK respuestaPublicacionPK) {
        this.respuestaPublicacionPK = respuestaPublicacionPK;
    }

    public RespuestaPublicacion(int id, int idPublicacion) {
        this.respuestaPublicacionPK = new RespuestaPublicacionPK(id, idPublicacion);
    }

    public RespuestaPublicacionPK getRespuestaPublicacionPK() {
        return respuestaPublicacionPK;
    }

    public void setRespuestaPublicacionPK(RespuestaPublicacionPK respuestaPublicacionPK) {
        this.respuestaPublicacionPK = respuestaPublicacionPK;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Motivo getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(Motivo idMotivo) {
        this.idMotivo = idMotivo;
    }

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Publicacion publicacion) {
        this.publicacion = publicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (respuestaPublicacionPK != null ? respuestaPublicacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaPublicacion)) {
            return false;
        }
        RespuestaPublicacion other = (RespuestaPublicacion) object;
        if ((this.respuestaPublicacionPK == null && other.respuestaPublicacionPK != null) || (this.respuestaPublicacionPK != null && !this.respuestaPublicacionPK.equals(other.respuestaPublicacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.RespuestaPublicacion[ respuestaPublicacionPK=" + respuestaPublicacionPK + " ]";
    }
    
}
