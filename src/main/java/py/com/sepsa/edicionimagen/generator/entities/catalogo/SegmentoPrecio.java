/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "segmento_precio", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SegmentoPrecio.findAll", query = "SELECT s FROM SegmentoPrecio s"),
    @NamedQuery(name = "SegmentoPrecio.findById", query = "SELECT s FROM SegmentoPrecio s WHERE s.id = :id"),
    @NamedQuery(name = "SegmentoPrecio.findByMetodoDistribucion", query = "SELECT s FROM SegmentoPrecio s WHERE s.metodoDistribucion = :metodoDistribucion"),
    @NamedQuery(name = "SegmentoPrecio.findByCodigoAccion", query = "SELECT s FROM SegmentoPrecio s WHERE s.codigoAccion = :codigoAccion"),
    @NamedQuery(name = "SegmentoPrecio.findByRazon", query = "SELECT s FROM SegmentoPrecio s WHERE s.razon = :razon"),
    @NamedQuery(name = "SegmentoPrecio.findBySecuenciaAplicacion", query = "SELECT s FROM SegmentoPrecio s WHERE s.secuenciaAplicacion = :secuenciaAplicacion"),
    @NamedQuery(name = "SegmentoPrecio.findByTipoPrecio", query = "SELECT s FROM SegmentoPrecio s WHERE s.tipoPrecio = :tipoPrecio"),
    @NamedQuery(name = "SegmentoPrecio.findByDescripcion", query = "SELECT s FROM SegmentoPrecio s WHERE s.descripcion = :descripcion"),
    @NamedQuery(name = "SegmentoPrecio.findByCantidadMinima", query = "SELECT s FROM SegmentoPrecio s WHERE s.cantidadMinima = :cantidadMinima"),
    @NamedQuery(name = "SegmentoPrecio.findByCantidadMinimaUom", query = "SELECT s FROM SegmentoPrecio s WHERE s.cantidadMinimaUom = :cantidadMinimaUom"),
    @NamedQuery(name = "SegmentoPrecio.findByPrecio", query = "SELECT s FROM SegmentoPrecio s WHERE s.precio = :precio"),
    @NamedQuery(name = "SegmentoPrecio.findByMonedaPrecio", query = "SELECT s FROM SegmentoPrecio s WHERE s.monedaPrecio = :monedaPrecio"),
    @NamedQuery(name = "SegmentoPrecio.findByPrecioSugerido", query = "SELECT s FROM SegmentoPrecio s WHERE s.precioSugerido = :precioSugerido"),
    @NamedQuery(name = "SegmentoPrecio.findByMonedaPrecioSugerido", query = "SELECT s FROM SegmentoPrecio s WHERE s.monedaPrecioSugerido = :monedaPrecioSugerido"),
    @NamedQuery(name = "SegmentoPrecio.findByTipoValorPrecio", query = "SELECT s FROM SegmentoPrecio s WHERE s.tipoValorPrecio = :tipoValorPrecio"),
    @NamedQuery(name = "SegmentoPrecio.findByCalificadorPrecio", query = "SELECT s FROM SegmentoPrecio s WHERE s.calificadorPrecio = :calificadorPrecio"),
    @NamedQuery(name = "SegmentoPrecio.findByIdDocumento", query = "SELECT s FROM SegmentoPrecio s WHERE s.idDocumento = :idDocumento"),
    @NamedQuery(name = "SegmentoPrecio.findByIdTipoDocumento", query = "SELECT s FROM SegmentoPrecio s WHERE s.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "SegmentoPrecio.findByPrecioTotal", query = "SELECT s FROM SegmentoPrecio s WHERE s.precioTotal = :precioTotal"),
    @NamedQuery(name = "SegmentoPrecio.findByMonedaPrecioTotal", query = "SELECT s FROM SegmentoPrecio s WHERE s.monedaPrecioTotal = :monedaPrecioTotal")})
public class SegmentoPrecio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "metodo_distribucion")
    private String metodoDistribucion;
    @Basic(optional = false)
    @Column(name = "codigo_accion")
    private String codigoAccion;
    @Column(name = "razon")
    private String razon;
    @Basic(optional = false)
    @Column(name = "secuencia_aplicacion")
    private int secuenciaAplicacion;
    @Basic(optional = false)
    @Column(name = "tipo_precio")
    private String tipoPrecio;
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "cantidad_minima")
    private double cantidadMinima;
    @Basic(optional = false)
    @Column(name = "cantidad_minima_uom")
    private String cantidadMinimaUom;
    @Basic(optional = false)
    @Column(name = "precio")
    private double precio;
    @Basic(optional = false)
    @Column(name = "moneda_precio")
    private String monedaPrecio;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio_sugerido")
    private Double precioSugerido;
    @Column(name = "moneda_precio_sugerido")
    private String monedaPrecioSugerido;
    @Basic(optional = false)
    @Column(name = "tipo_valor_precio")
    private String tipoValorPrecio;
    @Column(name = "calificador_precio")
    private String calificadorPrecio;
    @Basic(optional = false)
    @Column(name = "id_documento")
    private int idDocumento;
    @Basic(optional = false)
    @Column(name = "id_tipo_documento")
    private int idTipoDocumento;
    @Column(name = "precio_total")
    private Double precioTotal;
    @Column(name = "moneda_precio_total")
    private String monedaPrecioTotal;
    @OneToMany(mappedBy = "idSegmentoPrecio")
    private Collection<DetalleConfirmacion> detalleConfirmacionCollection;
    @JoinColumn(name = "id_area", referencedColumnName = "id")
    @ManyToOne
    private Area idArea;
    @JoinColumn(name = "id_calificador_descripcion_articulo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CalificadorDescripcionArticulo idCalificadorDescripcionArticulo;
    @JoinColumn(name = "id_notificacion", referencedColumnName = "id")
    @ManyToOne
    private Notificacion idNotificacion;
    @JoinColumn(name = "id_sector", referencedColumnName = "id")
    @ManyToOne
    private Sector idSector;
    @OneToMany(mappedBy = "idRelSegmentoPrecio")
    private Collection<SegmentoPrecio> segmentoPrecioCollection;
    @JoinColumn(name = "id_rel_segmento_precio", referencedColumnName = "id")
    @ManyToOne
    private SegmentoPrecio idRelSegmentoPrecio;
    @JoinColumn(name = "id_relacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private SegmentoRelacion idRelacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSegmentoPrecio")
    private Collection<DocumentoReferencia> documentoReferenciaCollection;
    @OneToMany(mappedBy = "idSegmentoPrecio")
    private Collection<FechaEfectivaInicio> fechaEfectivaInicioCollection;
    @OneToMany(mappedBy = "idSegmentoPrecio")
    private Collection<UsuarioMensaje> usuarioMensajeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segmentoPrecio")
    private Collection<SegmentoPrecioEtiqueta> segmentoPrecioEtiquetaCollection;
    @OneToMany(mappedBy = "idSegmentoPrecio")
    private Collection<FechaEfectivaFin> fechaEfectivaFinCollection;

    public SegmentoPrecio() {
    }

    public SegmentoPrecio(Integer id) {
        this.id = id;
    }

    public SegmentoPrecio(Integer id, String metodoDistribucion, String codigoAccion, int secuenciaAplicacion, String tipoPrecio, double cantidadMinima, String cantidadMinimaUom, double precio, String monedaPrecio, String tipoValorPrecio, int idDocumento, int idTipoDocumento) {
        this.id = id;
        this.metodoDistribucion = metodoDistribucion;
        this.codigoAccion = codigoAccion;
        this.secuenciaAplicacion = secuenciaAplicacion;
        this.tipoPrecio = tipoPrecio;
        this.cantidadMinima = cantidadMinima;
        this.cantidadMinimaUom = cantidadMinimaUom;
        this.precio = precio;
        this.monedaPrecio = monedaPrecio;
        this.tipoValorPrecio = tipoValorPrecio;
        this.idDocumento = idDocumento;
        this.idTipoDocumento = idTipoDocumento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMetodoDistribucion() {
        return metodoDistribucion;
    }

    public void setMetodoDistribucion(String metodoDistribucion) {
        this.metodoDistribucion = metodoDistribucion;
    }

    public String getCodigoAccion() {
        return codigoAccion;
    }

    public void setCodigoAccion(String codigoAccion) {
        this.codigoAccion = codigoAccion;
    }

    public String getRazon() {
        return razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public int getSecuenciaAplicacion() {
        return secuenciaAplicacion;
    }

    public void setSecuenciaAplicacion(int secuenciaAplicacion) {
        this.secuenciaAplicacion = secuenciaAplicacion;
    }

    public String getTipoPrecio() {
        return tipoPrecio;
    }

    public void setTipoPrecio(String tipoPrecio) {
        this.tipoPrecio = tipoPrecio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getCantidadMinima() {
        return cantidadMinima;
    }

    public void setCantidadMinima(double cantidadMinima) {
        this.cantidadMinima = cantidadMinima;
    }

    public String getCantidadMinimaUom() {
        return cantidadMinimaUom;
    }

    public void setCantidadMinimaUom(String cantidadMinimaUom) {
        this.cantidadMinimaUom = cantidadMinimaUom;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getMonedaPrecio() {
        return monedaPrecio;
    }

    public void setMonedaPrecio(String monedaPrecio) {
        this.monedaPrecio = monedaPrecio;
    }

    public Double getPrecioSugerido() {
        return precioSugerido;
    }

    public void setPrecioSugerido(Double precioSugerido) {
        this.precioSugerido = precioSugerido;
    }

    public String getMonedaPrecioSugerido() {
        return monedaPrecioSugerido;
    }

    public void setMonedaPrecioSugerido(String monedaPrecioSugerido) {
        this.monedaPrecioSugerido = monedaPrecioSugerido;
    }

    public String getTipoValorPrecio() {
        return tipoValorPrecio;
    }

    public void setTipoValorPrecio(String tipoValorPrecio) {
        this.tipoValorPrecio = tipoValorPrecio;
    }

    public String getCalificadorPrecio() {
        return calificadorPrecio;
    }

    public void setCalificadorPrecio(String calificadorPrecio) {
        this.calificadorPrecio = calificadorPrecio;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public String getMonedaPrecioTotal() {
        return monedaPrecioTotal;
    }

    public void setMonedaPrecioTotal(String monedaPrecioTotal) {
        this.monedaPrecioTotal = monedaPrecioTotal;
    }

    @XmlTransient
    public Collection<DetalleConfirmacion> getDetalleConfirmacionCollection() {
        return detalleConfirmacionCollection;
    }

    public void setDetalleConfirmacionCollection(Collection<DetalleConfirmacion> detalleConfirmacionCollection) {
        this.detalleConfirmacionCollection = detalleConfirmacionCollection;
    }

    public Area getIdArea() {
        return idArea;
    }

    public void setIdArea(Area idArea) {
        this.idArea = idArea;
    }

    public CalificadorDescripcionArticulo getIdCalificadorDescripcionArticulo() {
        return idCalificadorDescripcionArticulo;
    }

    public void setIdCalificadorDescripcionArticulo(CalificadorDescripcionArticulo idCalificadorDescripcionArticulo) {
        this.idCalificadorDescripcionArticulo = idCalificadorDescripcionArticulo;
    }

    public Notificacion getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(Notificacion idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public Sector getIdSector() {
        return idSector;
    }

    public void setIdSector(Sector idSector) {
        this.idSector = idSector;
    }

    @XmlTransient
    public Collection<SegmentoPrecio> getSegmentoPrecioCollection() {
        return segmentoPrecioCollection;
    }

    public void setSegmentoPrecioCollection(Collection<SegmentoPrecio> segmentoPrecioCollection) {
        this.segmentoPrecioCollection = segmentoPrecioCollection;
    }

    public SegmentoPrecio getIdRelSegmentoPrecio() {
        return idRelSegmentoPrecio;
    }

    public void setIdRelSegmentoPrecio(SegmentoPrecio idRelSegmentoPrecio) {
        this.idRelSegmentoPrecio = idRelSegmentoPrecio;
    }

    public SegmentoRelacion getIdRelacion() {
        return idRelacion;
    }

    public void setIdRelacion(SegmentoRelacion idRelacion) {
        this.idRelacion = idRelacion;
    }

    @XmlTransient
    public Collection<DocumentoReferencia> getDocumentoReferenciaCollection() {
        return documentoReferenciaCollection;
    }

    public void setDocumentoReferenciaCollection(Collection<DocumentoReferencia> documentoReferenciaCollection) {
        this.documentoReferenciaCollection = documentoReferenciaCollection;
    }

    @XmlTransient
    public Collection<FechaEfectivaInicio> getFechaEfectivaInicioCollection() {
        return fechaEfectivaInicioCollection;
    }

    public void setFechaEfectivaInicioCollection(Collection<FechaEfectivaInicio> fechaEfectivaInicioCollection) {
        this.fechaEfectivaInicioCollection = fechaEfectivaInicioCollection;
    }

    @XmlTransient
    public Collection<UsuarioMensaje> getUsuarioMensajeCollection() {
        return usuarioMensajeCollection;
    }

    public void setUsuarioMensajeCollection(Collection<UsuarioMensaje> usuarioMensajeCollection) {
        this.usuarioMensajeCollection = usuarioMensajeCollection;
    }

    @XmlTransient
    public Collection<SegmentoPrecioEtiqueta> getSegmentoPrecioEtiquetaCollection() {
        return segmentoPrecioEtiquetaCollection;
    }

    public void setSegmentoPrecioEtiquetaCollection(Collection<SegmentoPrecioEtiqueta> segmentoPrecioEtiquetaCollection) {
        this.segmentoPrecioEtiquetaCollection = segmentoPrecioEtiquetaCollection;
    }

    @XmlTransient
    public Collection<FechaEfectivaFin> getFechaEfectivaFinCollection() {
        return fechaEfectivaFinCollection;
    }

    public void setFechaEfectivaFinCollection(Collection<FechaEfectivaFin> fechaEfectivaFinCollection) {
        this.fechaEfectivaFinCollection = fechaEfectivaFinCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SegmentoPrecio)) {
            return false;
        }
        SegmentoPrecio other = (SegmentoPrecio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.SegmentoPrecio[ id=" + id + " ]";
    }
    
}
