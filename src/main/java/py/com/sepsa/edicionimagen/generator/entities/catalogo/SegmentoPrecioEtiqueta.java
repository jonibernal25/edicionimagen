/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "segmento_precio_etiqueta", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SegmentoPrecioEtiqueta.findAll", query = "SELECT s FROM SegmentoPrecioEtiqueta s"),
    @NamedQuery(name = "SegmentoPrecioEtiqueta.findByIdSegmentoPrecio", query = "SELECT s FROM SegmentoPrecioEtiqueta s WHERE s.segmentoPrecioEtiquetaPK.idSegmentoPrecio = :idSegmentoPrecio"),
    @NamedQuery(name = "SegmentoPrecioEtiqueta.findByIdEtiqueta", query = "SELECT s FROM SegmentoPrecioEtiqueta s WHERE s.segmentoPrecioEtiquetaPK.idEtiqueta = :idEtiqueta"),
    @NamedQuery(name = "SegmentoPrecioEtiqueta.findByIdUsuario", query = "SELECT s FROM SegmentoPrecioEtiqueta s WHERE s.idUsuario = :idUsuario"),
    @NamedQuery(name = "SegmentoPrecioEtiqueta.findByFecha", query = "SELECT s FROM SegmentoPrecioEtiqueta s WHERE s.fecha = :fecha"),
    @NamedQuery(name = "SegmentoPrecioEtiqueta.findByObservacion", query = "SELECT s FROM SegmentoPrecioEtiqueta s WHERE s.observacion = :observacion")})
public class SegmentoPrecioEtiqueta implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SegmentoPrecioEtiquetaPK segmentoPrecioEtiquetaPK;
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private int idUsuario;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "observacion")
    private String observacion;
    @JoinColumn(name = "id_etiqueta", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Etiqueta etiqueta;
    @JoinColumn(name = "id_segmento_precio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SegmentoPrecio segmentoPrecio;

    public SegmentoPrecioEtiqueta() {
    }

    public SegmentoPrecioEtiqueta(SegmentoPrecioEtiquetaPK segmentoPrecioEtiquetaPK) {
        this.segmentoPrecioEtiquetaPK = segmentoPrecioEtiquetaPK;
    }

    public SegmentoPrecioEtiqueta(SegmentoPrecioEtiquetaPK segmentoPrecioEtiquetaPK, int idUsuario, Date fecha) {
        this.segmentoPrecioEtiquetaPK = segmentoPrecioEtiquetaPK;
        this.idUsuario = idUsuario;
        this.fecha = fecha;
    }

    public SegmentoPrecioEtiqueta(int idSegmentoPrecio, int idEtiqueta) {
        this.segmentoPrecioEtiquetaPK = new SegmentoPrecioEtiquetaPK(idSegmentoPrecio, idEtiqueta);
    }

    public SegmentoPrecioEtiquetaPK getSegmentoPrecioEtiquetaPK() {
        return segmentoPrecioEtiquetaPK;
    }

    public void setSegmentoPrecioEtiquetaPK(SegmentoPrecioEtiquetaPK segmentoPrecioEtiquetaPK) {
        this.segmentoPrecioEtiquetaPK = segmentoPrecioEtiquetaPK;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Etiqueta getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(Etiqueta etiqueta) {
        this.etiqueta = etiqueta;
    }

    public SegmentoPrecio getSegmentoPrecio() {
        return segmentoPrecio;
    }

    public void setSegmentoPrecio(SegmentoPrecio segmentoPrecio) {
        this.segmentoPrecio = segmentoPrecio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segmentoPrecioEtiquetaPK != null ? segmentoPrecioEtiquetaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SegmentoPrecioEtiqueta)) {
            return false;
        }
        SegmentoPrecioEtiqueta other = (SegmentoPrecioEtiqueta) object;
        if ((this.segmentoPrecioEtiquetaPK == null && other.segmentoPrecioEtiquetaPK != null) || (this.segmentoPrecioEtiquetaPK != null && !this.segmentoPrecioEtiquetaPK.equals(other.segmentoPrecioEtiquetaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.SegmentoPrecioEtiqueta[ segmentoPrecioEtiquetaPK=" + segmentoPrecioEtiquetaPK + " ]";
    }
    
}
