/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "segmento_relacion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SegmentoRelacion.findAll", query = "SELECT s FROM SegmentoRelacion s"),
    @NamedQuery(name = "SegmentoRelacion.findById", query = "SELECT s FROM SegmentoRelacion s WHERE s.id = :id"),
    @NamedQuery(name = "SegmentoRelacion.findByIdDocumento", query = "SELECT s FROM SegmentoRelacion s WHERE s.idDocumento = :idDocumento"),
    @NamedQuery(name = "SegmentoRelacion.findByIdTipoDocumento", query = "SELECT s FROM SegmentoRelacion s WHERE s.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "SegmentoRelacion.findByMoneda", query = "SELECT s FROM SegmentoRelacion s WHERE s.moneda = :moneda"),
    @NamedQuery(name = "SegmentoRelacion.findByCodigoAccion", query = "SELECT s FROM SegmentoRelacion s WHERE s.codigoAccion = :codigoAccion"),
    @NamedQuery(name = "SegmentoRelacion.findByFechaInicio", query = "SELECT s FROM SegmentoRelacion s WHERE s.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "SegmentoRelacion.findByFechaFin", query = "SELECT s FROM SegmentoRelacion s WHERE s.fechaFin = :fechaFin"),
    @NamedQuery(name = "SegmentoRelacion.findByCanalComercio", query = "SELECT s FROM SegmentoRelacion s WHERE s.canalComercio = :canalComercio"),
    @NamedQuery(name = "SegmentoRelacion.findByFechaModificacion", query = "SELECT s FROM SegmentoRelacion s WHERE s.fechaModificacion = :fechaModificacion"),
    @NamedQuery(name = "SegmentoRelacion.findByDataSource", query = "SELECT s FROM SegmentoRelacion s WHERE s.dataSource = :dataSource"),
    @NamedQuery(name = "SegmentoRelacion.findByDataRecipient", query = "SELECT s FROM SegmentoRelacion s WHERE s.dataRecipient = :dataRecipient"),
    @NamedQuery(name = "SegmentoRelacion.findByDocNum", query = "SELECT s FROM SegmentoRelacion s WHERE s.docNum = :docNum")})
public class SegmentoRelacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_documento")
    private int idDocumento;
    @Basic(optional = false)
    @Column(name = "id_tipo_documento")
    private int idTipoDocumento;
    @Basic(optional = false)
    @Column(name = "moneda")
    private String moneda;
    @Basic(optional = false)
    @Column(name = "codigo_accion")
    private String codigoAccion;
    @Basic(optional = false)
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @Basic(optional = false)
    @Column(name = "canal_comercio")
    private String canalComercio;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Basic(optional = false)
    @Column(name = "data_source")
    private BigInteger dataSource;
    @Basic(optional = false)
    @Column(name = "data_recipient")
    private BigInteger dataRecipient;
    @Basic(optional = false)
    @Column(name = "doc_num")
    private String docNum;
    @OneToMany(mappedBy = "idSegmentoRelacion")
    private Collection<DetalleConfirmacion> detalleConfirmacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRelacion")
    private Collection<SegmentoPrecio> segmentoPrecioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRelacion")
    private Collection<CalificadorDescripcionArticulo> calificadorDescripcionArticuloCollection;

    public SegmentoRelacion() {
    }

    public SegmentoRelacion(Integer id) {
        this.id = id;
    }

    public SegmentoRelacion(Integer id, int idDocumento, int idTipoDocumento, String moneda, String codigoAccion, Date fechaInicio, String canalComercio, Date fechaModificacion, BigInteger dataSource, BigInteger dataRecipient, String docNum) {
        this.id = id;
        this.idDocumento = idDocumento;
        this.idTipoDocumento = idTipoDocumento;
        this.moneda = moneda;
        this.codigoAccion = codigoAccion;
        this.fechaInicio = fechaInicio;
        this.canalComercio = canalComercio;
        this.fechaModificacion = fechaModificacion;
        this.dataSource = dataSource;
        this.dataRecipient = dataRecipient;
        this.docNum = docNum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getCodigoAccion() {
        return codigoAccion;
    }

    public void setCodigoAccion(String codigoAccion) {
        this.codigoAccion = codigoAccion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getCanalComercio() {
        return canalComercio;
    }

    public void setCanalComercio(String canalComercio) {
        this.canalComercio = canalComercio;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public BigInteger getDataSource() {
        return dataSource;
    }

    public void setDataSource(BigInteger dataSource) {
        this.dataSource = dataSource;
    }

    public BigInteger getDataRecipient() {
        return dataRecipient;
    }

    public void setDataRecipient(BigInteger dataRecipient) {
        this.dataRecipient = dataRecipient;
    }

    public String getDocNum() {
        return docNum;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    @XmlTransient
    public Collection<DetalleConfirmacion> getDetalleConfirmacionCollection() {
        return detalleConfirmacionCollection;
    }

    public void setDetalleConfirmacionCollection(Collection<DetalleConfirmacion> detalleConfirmacionCollection) {
        this.detalleConfirmacionCollection = detalleConfirmacionCollection;
    }

    @XmlTransient
    public Collection<SegmentoPrecio> getSegmentoPrecioCollection() {
        return segmentoPrecioCollection;
    }

    public void setSegmentoPrecioCollection(Collection<SegmentoPrecio> segmentoPrecioCollection) {
        this.segmentoPrecioCollection = segmentoPrecioCollection;
    }

    @XmlTransient
    public Collection<CalificadorDescripcionArticulo> getCalificadorDescripcionArticuloCollection() {
        return calificadorDescripcionArticuloCollection;
    }

    public void setCalificadorDescripcionArticuloCollection(Collection<CalificadorDescripcionArticulo> calificadorDescripcionArticuloCollection) {
        this.calificadorDescripcionArticuloCollection = calificadorDescripcionArticuloCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SegmentoRelacion)) {
            return false;
        }
        SegmentoRelacion other = (SegmentoRelacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.SegmentoRelacion[ id=" + id + " ]";
    }
    
}
