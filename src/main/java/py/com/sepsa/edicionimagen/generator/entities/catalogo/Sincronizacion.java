/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "sincronizacion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sincronizacion.findAll", query = "SELECT s FROM Sincronizacion s"),
    @NamedQuery(name = "Sincronizacion.findById", query = "SELECT s FROM Sincronizacion s WHERE s.id = :id"),
    @NamedQuery(name = "Sincronizacion.findBySourceGtin", query = "SELECT s FROM Sincronizacion s WHERE s.sourceGtin = :sourceGtin"),
    @NamedQuery(name = "Sincronizacion.findByEstado", query = "SELECT s FROM Sincronizacion s WHERE s.estado = :estado"),
    @NamedQuery(name = "Sincronizacion.findBySourceGln", query = "SELECT s FROM Sincronizacion s WHERE s.sourceGln = :sourceGln"),
    @NamedQuery(name = "Sincronizacion.findByRecipientGln", query = "SELECT s FROM Sincronizacion s WHERE s.recipientGln = :recipientGln"),
    @NamedQuery(name = "Sincronizacion.findByRdpGln", query = "SELECT s FROM Sincronizacion s WHERE s.rdpGln = :rdpGln")})
public class Sincronizacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "source_gtin")
    private String sourceGtin;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Basic(optional = false)
    @Column(name = "source_gln")
    private BigInteger sourceGln;
    @Basic(optional = false)
    @Column(name = "recipient_gln")
    private BigInteger recipientGln;
    @Basic(optional = false)
    @Column(name = "rdp_gln")
    private BigInteger rdpGln;
    @JoinColumn(name = "id_codigo_pais", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CodigoPais idCodigoPais;
    @JoinColumn(name = "id_codigo_subdivision", referencedColumnName = "id")
    @ManyToOne
    private CodigoSubdivision idCodigoSubdivision;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "sincronizacion")
    private Notificacion notificacion;

    public Sincronizacion() {
    }

    public Sincronizacion(Integer id) {
        this.id = id;
    }

    public Sincronizacion(Integer id, String sourceGtin, Character estado, BigInteger sourceGln, BigInteger recipientGln, BigInteger rdpGln) {
        this.id = id;
        this.sourceGtin = sourceGtin;
        this.estado = estado;
        this.sourceGln = sourceGln;
        this.recipientGln = recipientGln;
        this.rdpGln = rdpGln;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSourceGtin() {
        return sourceGtin;
    }

    public void setSourceGtin(String sourceGtin) {
        this.sourceGtin = sourceGtin;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public BigInteger getSourceGln() {
        return sourceGln;
    }

    public void setSourceGln(BigInteger sourceGln) {
        this.sourceGln = sourceGln;
    }

    public BigInteger getRecipientGln() {
        return recipientGln;
    }

    public void setRecipientGln(BigInteger recipientGln) {
        this.recipientGln = recipientGln;
    }

    public BigInteger getRdpGln() {
        return rdpGln;
    }

    public void setRdpGln(BigInteger rdpGln) {
        this.rdpGln = rdpGln;
    }

    public CodigoPais getIdCodigoPais() {
        return idCodigoPais;
    }

    public void setIdCodigoPais(CodigoPais idCodigoPais) {
        this.idCodigoPais = idCodigoPais;
    }

    public CodigoSubdivision getIdCodigoSubdivision() {
        return idCodigoSubdivision;
    }

    public void setIdCodigoSubdivision(CodigoSubdivision idCodigoSubdivision) {
        this.idCodigoSubdivision = idCodigoSubdivision;
    }

    public Notificacion getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(Notificacion notificacion) {
        this.notificacion = notificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sincronizacion)) {
            return false;
        }
        Sincronizacion other = (Sincronizacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Sincronizacion[ id=" + id + " ]";
    }
    
}
