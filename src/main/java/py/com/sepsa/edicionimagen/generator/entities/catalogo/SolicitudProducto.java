/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "solicitud_producto", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SolicitudProducto.findAll", query = "SELECT s FROM SolicitudProducto s"),
    @NamedQuery(name = "SolicitudProducto.findById", query = "SELECT s FROM SolicitudProducto s WHERE s.id = :id"),
    @NamedQuery(name = "SolicitudProducto.findByIdComprador", query = "SELECT s FROM SolicitudProducto s WHERE s.idComprador = :idComprador"),
    @NamedQuery(name = "SolicitudProducto.findByIdProducto", query = "SELECT s FROM SolicitudProducto s WHERE s.idProducto = :idProducto"),
    @NamedQuery(name = "SolicitudProducto.findByIdDocumento", query = "SELECT s FROM SolicitudProducto s WHERE s.idDocumento = :idDocumento"),
    @NamedQuery(name = "SolicitudProducto.findByIdTipoDocumento", query = "SELECT s FROM SolicitudProducto s WHERE s.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "SolicitudProducto.findByFechaSolicitud", query = "SELECT s FROM SolicitudProducto s WHERE s.fechaSolicitud = :fechaSolicitud"),
    @NamedQuery(name = "SolicitudProducto.findByFechaProceso", query = "SELECT s FROM SolicitudProducto s WHERE s.fechaProceso = :fechaProceso"),
    @NamedQuery(name = "SolicitudProducto.findByEstado", query = "SELECT s FROM SolicitudProducto s WHERE s.estado = :estado")})
public class SolicitudProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_comprador")
    private Integer idComprador;
    @Column(name = "id_producto")
    private Integer idProducto;
    @Column(name = "id_documento")
    private Integer idDocumento;
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;
    @Basic(optional = false)
    @Column(name = "fecha_solicitud")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSolicitud;
    @Column(name = "fecha_proceso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaProceso;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;

    public SolicitudProducto() {
    }

    public SolicitudProducto(Integer id) {
        this.id = id;
    }

    public SolicitudProducto(Integer id, Date fechaSolicitud, Character estado) {
        this.id = id;
        this.fechaSolicitud = fechaSolicitud;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Date getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public Date getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudProducto)) {
            return false;
        }
        SolicitudProducto other = (SolicitudProducto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.SolicitudProducto[ id=" + id + " ]";
    }
    
}
