/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "subscripcion", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Subscripcion.findAll", query = "SELECT s FROM Subscripcion s"),
    @NamedQuery(name = "Subscripcion.findById", query = "SELECT s FROM Subscripcion s WHERE s.id = :id"),
    @NamedQuery(name = "Subscripcion.findBySourceGtin", query = "SELECT s FROM Subscripcion s WHERE s.sourceGtin = :sourceGtin"),
    @NamedQuery(name = "Subscripcion.findByEstado", query = "SELECT s FROM Subscripcion s WHERE s.estado = :estado"),
    @NamedQuery(name = "Subscripcion.findBySourceGln", query = "SELECT s FROM Subscripcion s WHERE s.sourceGln = :sourceGln"),
    @NamedQuery(name = "Subscripcion.findByRecipientGln", query = "SELECT s FROM Subscripcion s WHERE s.recipientGln = :recipientGln"),
    @NamedQuery(name = "Subscripcion.findByRdpGln", query = "SELECT s FROM Subscripcion s WHERE s.rdpGln = :rdpGln")})
public class Subscripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "source_gtin")
    private String sourceGtin;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "source_gln")
    private BigInteger sourceGln;
    @Basic(optional = false)
    @Column(name = "recipient_gln")
    private BigInteger recipientGln;
    @Basic(optional = false)
    @Column(name = "rdp_gln")
    private BigInteger rdpGln;
    @JoinColumn(name = "id_codigo_pais", referencedColumnName = "id")
    @ManyToOne
    private CodigoPais idCodigoPais;
    @JoinColumn(name = "id_codigo_subdivision", referencedColumnName = "id")
    @ManyToOne
    private CodigoSubdivision idCodigoSubdivision;

    public Subscripcion() {
    }

    public Subscripcion(Integer id) {
        this.id = id;
    }

    public Subscripcion(Integer id, Character estado, BigInteger recipientGln, BigInteger rdpGln) {
        this.id = id;
        this.estado = estado;
        this.recipientGln = recipientGln;
        this.rdpGln = rdpGln;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSourceGtin() {
        return sourceGtin;
    }

    public void setSourceGtin(String sourceGtin) {
        this.sourceGtin = sourceGtin;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public BigInteger getSourceGln() {
        return sourceGln;
    }

    public void setSourceGln(BigInteger sourceGln) {
        this.sourceGln = sourceGln;
    }

    public BigInteger getRecipientGln() {
        return recipientGln;
    }

    public void setRecipientGln(BigInteger recipientGln) {
        this.recipientGln = recipientGln;
    }

    public BigInteger getRdpGln() {
        return rdpGln;
    }

    public void setRdpGln(BigInteger rdpGln) {
        this.rdpGln = rdpGln;
    }

    public CodigoPais getIdCodigoPais() {
        return idCodigoPais;
    }

    public void setIdCodigoPais(CodigoPais idCodigoPais) {
        this.idCodigoPais = idCodigoPais;
    }

    public CodigoSubdivision getIdCodigoSubdivision() {
        return idCodigoSubdivision;
    }

    public void setIdCodigoSubdivision(CodigoSubdivision idCodigoSubdivision) {
        this.idCodigoSubdivision = idCodigoSubdivision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subscripcion)) {
            return false;
        }
        Subscripcion other = (Subscripcion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.Subscripcion[ id=" + id + " ]";
    }
    
}
