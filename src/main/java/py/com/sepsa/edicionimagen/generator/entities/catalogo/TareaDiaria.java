/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "tarea_diaria", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TareaDiaria.findAll", query = "SELECT t FROM TareaDiaria t"),
    @NamedQuery(name = "TareaDiaria.findById", query = "SELECT t FROM TareaDiaria t WHERE t.id = :id"),
    @NamedQuery(name = "TareaDiaria.findByInicioDia", query = "SELECT t FROM TareaDiaria t WHERE t.inicioDia = :inicioDia"),
    @NamedQuery(name = "TareaDiaria.findByFinDia", query = "SELECT t FROM TareaDiaria t WHERE t.finDia = :finDia"),
    @NamedQuery(name = "TareaDiaria.findByIdUsuario", query = "SELECT t FROM TareaDiaria t WHERE t.idUsuario = :idUsuario")})
public class TareaDiaria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "inicio_dia")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioDia;
    @Column(name = "fin_dia")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finDia;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTareaDiaria")
    private Collection<TareaDiariaDetalle> tareaDiariaDetalleCollection;

    public TareaDiaria() {
    }

    public TareaDiaria(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getInicioDia() {
        return inicioDia;
    }

    public void setInicioDia(Date inicioDia) {
        this.inicioDia = inicioDia;
    }

    public Date getFinDia() {
        return finDia;
    }

    public void setFinDia(Date finDia) {
        this.finDia = finDia;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @XmlTransient
    public Collection<TareaDiariaDetalle> getTareaDiariaDetalleCollection() {
        return tareaDiariaDetalleCollection;
    }

    public void setTareaDiariaDetalleCollection(Collection<TareaDiariaDetalle> tareaDiariaDetalleCollection) {
        this.tareaDiariaDetalleCollection = tareaDiariaDetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TareaDiaria)) {
            return false;
        }
        TareaDiaria other = (TareaDiaria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.TareaDiaria[ id=" + id + " ]";
    }
    
}
