/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "tarea_diaria_detalle", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TareaDiariaDetalle.findAll", query = "SELECT t FROM TareaDiariaDetalle t"),
    @NamedQuery(name = "TareaDiariaDetalle.findById", query = "SELECT t FROM TareaDiariaDetalle t WHERE t.id = :id"),
    @NamedQuery(name = "TareaDiariaDetalle.findByInicioPausa", query = "SELECT t FROM TareaDiariaDetalle t WHERE t.inicioPausa = :inicioPausa"),
    @NamedQuery(name = "TareaDiariaDetalle.findByFinPausa", query = "SELECT t FROM TareaDiariaDetalle t WHERE t.finPausa = :finPausa")})
public class TareaDiariaDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "inicio_pausa")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioPausa;
    @Column(name = "fin_pausa")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finPausa;
    @JoinColumn(name = "id_motivo", referencedColumnName = "id")
    @ManyToOne
    private MotivoPausa idMotivo;
    @JoinColumn(name = "id_tarea_diaria", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TareaDiaria idTareaDiaria;

    public TareaDiariaDetalle() {
    }

    public TareaDiariaDetalle(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getInicioPausa() {
        return inicioPausa;
    }

    public void setInicioPausa(Date inicioPausa) {
        this.inicioPausa = inicioPausa;
    }

    public Date getFinPausa() {
        return finPausa;
    }

    public void setFinPausa(Date finPausa) {
        this.finPausa = finPausa;
    }

    public MotivoPausa getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(MotivoPausa idMotivo) {
        this.idMotivo = idMotivo;
    }

    public TareaDiaria getIdTareaDiaria() {
        return idTareaDiaria;
    }

    public void setIdTareaDiaria(TareaDiaria idTareaDiaria) {
        this.idTareaDiaria = idTareaDiaria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TareaDiariaDetalle)) {
            return false;
        }
        TareaDiariaDetalle other = (TareaDiariaDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.TareaDiariaDetalle[ id=" + id + " ]";
    }
    
}
