/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "usuario_gerencia", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioGerencia.findAll", query = "SELECT u FROM UsuarioGerencia u"),
    @NamedQuery(name = "UsuarioGerencia.findByIdUsuario", query = "SELECT u FROM UsuarioGerencia u WHERE u.usuarioGerenciaPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "UsuarioGerencia.findByIdGerencia", query = "SELECT u FROM UsuarioGerencia u WHERE u.usuarioGerenciaPK.idGerencia = :idGerencia")})
public class UsuarioGerencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsuarioGerenciaPK usuarioGerenciaPK;
    @JoinColumn(name = "id_gerencia", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Gerencia gerencia;

    public UsuarioGerencia() {
    }

    public UsuarioGerencia(UsuarioGerenciaPK usuarioGerenciaPK) {
        this.usuarioGerenciaPK = usuarioGerenciaPK;
    }

    public UsuarioGerencia(int idUsuario, int idGerencia) {
        this.usuarioGerenciaPK = new UsuarioGerenciaPK(idUsuario, idGerencia);
    }

    public UsuarioGerenciaPK getUsuarioGerenciaPK() {
        return usuarioGerenciaPK;
    }

    public void setUsuarioGerenciaPK(UsuarioGerenciaPK usuarioGerenciaPK) {
        this.usuarioGerenciaPK = usuarioGerenciaPK;
    }

    public Gerencia getGerencia() {
        return gerencia;
    }

    public void setGerencia(Gerencia gerencia) {
        this.gerencia = gerencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioGerenciaPK != null ? usuarioGerenciaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioGerencia)) {
            return false;
        }
        UsuarioGerencia other = (UsuarioGerencia) object;
        if ((this.usuarioGerenciaPK == null && other.usuarioGerenciaPK != null) || (this.usuarioGerenciaPK != null && !this.usuarioGerenciaPK.equals(other.usuarioGerenciaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.UsuarioGerencia[ usuarioGerenciaPK=" + usuarioGerenciaPK + " ]";
    }
    
}
