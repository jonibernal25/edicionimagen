/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class UsuarioGerenciaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_usuario")
    private int idUsuario;
    @Basic(optional = false)
    @Column(name = "id_gerencia")
    private int idGerencia;

    public UsuarioGerenciaPK() {
    }

    public UsuarioGerenciaPK(int idUsuario, int idGerencia) {
        this.idUsuario = idUsuario;
        this.idGerencia = idGerencia;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdGerencia() {
        return idGerencia;
    }

    public void setIdGerencia(int idGerencia) {
        this.idGerencia = idGerencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUsuario;
        hash += (int) idGerencia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioGerenciaPK)) {
            return false;
        }
        UsuarioGerenciaPK other = (UsuarioGerenciaPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idGerencia != other.idGerencia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.UsuarioGerenciaPK[ idUsuario=" + idUsuario + ", idGerencia=" + idGerencia + " ]";
    }
    
}
