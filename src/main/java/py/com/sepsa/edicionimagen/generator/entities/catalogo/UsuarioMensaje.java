/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "usuario_mensaje", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioMensaje.findAll", query = "SELECT u FROM UsuarioMensaje u"),
    @NamedQuery(name = "UsuarioMensaje.findByIdUsuarioReceptor", query = "SELECT u FROM UsuarioMensaje u WHERE u.idUsuarioReceptor = :idUsuarioReceptor"),
    @NamedQuery(name = "UsuarioMensaje.findByEstado", query = "SELECT u FROM UsuarioMensaje u WHERE u.estado = :estado"),
    @NamedQuery(name = "UsuarioMensaje.findByCantidadMostrada", query = "SELECT u FROM UsuarioMensaje u WHERE u.cantidadMostrada = :cantidadMostrada"),
    @NamedQuery(name = "UsuarioMensaje.findByCantidadMostrar", query = "SELECT u FROM UsuarioMensaje u WHERE u.cantidadMostrar = :cantidadMostrar"),
    @NamedQuery(name = "UsuarioMensaje.findById", query = "SELECT u FROM UsuarioMensaje u WHERE u.id = :id"),
    @NamedQuery(name = "UsuarioMensaje.findByObservacion", query = "SELECT u FROM UsuarioMensaje u WHERE u.observacion = :observacion"),
    @NamedQuery(name = "UsuarioMensaje.findByIdUsuarioEmisor", query = "SELECT u FROM UsuarioMensaje u WHERE u.idUsuarioEmisor = :idUsuarioEmisor"),
    @NamedQuery(name = "UsuarioMensaje.findByIdProducto", query = "SELECT u FROM UsuarioMensaje u WHERE u.idProducto = :idProducto"),
    @NamedQuery(name = "UsuarioMensaje.findByFechaDesde", query = "SELECT u FROM UsuarioMensaje u WHERE u.fechaDesde = :fechaDesde"),
    @NamedQuery(name = "UsuarioMensaje.findByFechaHasta", query = "SELECT u FROM UsuarioMensaje u WHERE u.fechaHasta = :fechaHasta"),
    @NamedQuery(name = "UsuarioMensaje.findByIdPersonaReceptor", query = "SELECT u FROM UsuarioMensaje u WHERE u.idPersonaReceptor = :idPersonaReceptor"),
    @NamedQuery(name = "UsuarioMensaje.findByIdPersonaEmisor", query = "SELECT u FROM UsuarioMensaje u WHERE u.idPersonaEmisor = :idPersonaEmisor"),
    @NamedQuery(name = "UsuarioMensaje.findByFechaInsercion", query = "SELECT u FROM UsuarioMensaje u WHERE u.fechaInsercion = :fechaInsercion")})
public class UsuarioMensaje implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "id_usuario_receptor")
    private Integer idUsuarioReceptor;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Basic(optional = false)
    @Column(name = "cantidad_mostrada")
    private int cantidadMostrada;
    @Basic(optional = false)
    @Column(name = "cantidad_mostrar")
    private int cantidadMostrar;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "id_usuario_emisor")
    private Integer idUsuarioEmisor;
    @Column(name = "id_producto")
    private Integer idProducto;
    @Column(name = "fecha_desde")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaDesde;
    @Column(name = "fecha_hasta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHasta;
    @Basic(optional = false)
    @Column(name = "id_persona_receptor")
    private int idPersonaReceptor;
    @Basic(optional = false)
    @Column(name = "id_persona_emisor")
    private int idPersonaEmisor;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @JoinColumn(name = "id_confirmacion", referencedColumnName = "id")
    @ManyToOne
    private Confirmacion idConfirmacion;
    @JoinColumn(name = "id_mensaje", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Mensaje idMensaje;
    @JoinColumn(name = "id_notificacion", referencedColumnName = "id")
    @ManyToOne
    private Notificacion idNotificacion;
    @JoinColumn(name = "id_segmento_precio", referencedColumnName = "id")
    @ManyToOne
    private SegmentoPrecio idSegmentoPrecio;

    public UsuarioMensaje() {
    }

    public UsuarioMensaje(Integer id) {
        this.id = id;
    }

    public UsuarioMensaje(Integer id, Character estado, int cantidadMostrada, int cantidadMostrar, int idPersonaReceptor, int idPersonaEmisor) {
        this.id = id;
        this.estado = estado;
        this.cantidadMostrada = cantidadMostrada;
        this.cantidadMostrar = cantidadMostrar;
        this.idPersonaReceptor = idPersonaReceptor;
        this.idPersonaEmisor = idPersonaEmisor;
    }

    public Integer getIdUsuarioReceptor() {
        return idUsuarioReceptor;
    }

    public void setIdUsuarioReceptor(Integer idUsuarioReceptor) {
        this.idUsuarioReceptor = idUsuarioReceptor;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public int getCantidadMostrada() {
        return cantidadMostrada;
    }

    public void setCantidadMostrada(int cantidadMostrada) {
        this.cantidadMostrada = cantidadMostrada;
    }

    public int getCantidadMostrar() {
        return cantidadMostrar;
    }

    public void setCantidadMostrar(int cantidadMostrar) {
        this.cantidadMostrar = cantidadMostrar;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdUsuarioEmisor() {
        return idUsuarioEmisor;
    }

    public void setIdUsuarioEmisor(Integer idUsuarioEmisor) {
        this.idUsuarioEmisor = idUsuarioEmisor;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public int getIdPersonaReceptor() {
        return idPersonaReceptor;
    }

    public void setIdPersonaReceptor(int idPersonaReceptor) {
        this.idPersonaReceptor = idPersonaReceptor;
    }

    public int getIdPersonaEmisor() {
        return idPersonaEmisor;
    }

    public void setIdPersonaEmisor(int idPersonaEmisor) {
        this.idPersonaEmisor = idPersonaEmisor;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Confirmacion getIdConfirmacion() {
        return idConfirmacion;
    }

    public void setIdConfirmacion(Confirmacion idConfirmacion) {
        this.idConfirmacion = idConfirmacion;
    }

    public Mensaje getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(Mensaje idMensaje) {
        this.idMensaje = idMensaje;
    }

    public Notificacion getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(Notificacion idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

    public SegmentoPrecio getIdSegmentoPrecio() {
        return idSegmentoPrecio;
    }

    public void setIdSegmentoPrecio(SegmentoPrecio idSegmentoPrecio) {
        this.idSegmentoPrecio = idSegmentoPrecio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioMensaje)) {
            return false;
        }
        UsuarioMensaje other = (UsuarioMensaje) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.UsuarioMensaje[ id=" + id + " ]";
    }
    
}
