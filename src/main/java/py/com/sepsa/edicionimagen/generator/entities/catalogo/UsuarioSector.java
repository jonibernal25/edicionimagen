/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "usuario_sector", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioSector.findAll", query = "SELECT u FROM UsuarioSector u"),
    @NamedQuery(name = "UsuarioSector.findByIdUsuario", query = "SELECT u FROM UsuarioSector u WHERE u.usuarioSectorPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "UsuarioSector.findByIdSector", query = "SELECT u FROM UsuarioSector u WHERE u.usuarioSectorPK.idSector = :idSector")})
public class UsuarioSector implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsuarioSectorPK usuarioSectorPK;
    @JoinColumn(name = "id_sector", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sector sector;

    public UsuarioSector() {
    }

    public UsuarioSector(UsuarioSectorPK usuarioSectorPK) {
        this.usuarioSectorPK = usuarioSectorPK;
    }

    public UsuarioSector(int idUsuario, int idSector) {
        this.usuarioSectorPK = new UsuarioSectorPK(idUsuario, idSector);
    }

    public UsuarioSectorPK getUsuarioSectorPK() {
        return usuarioSectorPK;
    }

    public void setUsuarioSectorPK(UsuarioSectorPK usuarioSectorPK) {
        this.usuarioSectorPK = usuarioSectorPK;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioSectorPK != null ? usuarioSectorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioSector)) {
            return false;
        }
        UsuarioSector other = (UsuarioSector) object;
        if ((this.usuarioSectorPK == null && other.usuarioSectorPK != null) || (this.usuarioSectorPK != null && !this.usuarioSectorPK.equals(other.usuarioSectorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.UsuarioSector[ usuarioSectorPK=" + usuarioSectorPK + " ]";
    }
    
}
