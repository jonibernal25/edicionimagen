/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "valor_caracteristica", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ValorCaracteristica.findAll", query = "SELECT v FROM ValorCaracteristica v"),
    @NamedQuery(name = "ValorCaracteristica.findById", query = "SELECT v FROM ValorCaracteristica v WHERE v.valorCaracteristicaPK.id = :id"),
    @NamedQuery(name = "ValorCaracteristica.findByIdProductoCaracteristica", query = "SELECT v FROM ValorCaracteristica v WHERE v.valorCaracteristicaPK.idProductoCaracteristica = :idProductoCaracteristica"),
    @NamedQuery(name = "ValorCaracteristica.findByValor", query = "SELECT v FROM ValorCaracteristica v WHERE v.valor = :valor"),
    @NamedQuery(name = "ValorCaracteristica.findByIdProveedor", query = "SELECT v FROM ValorCaracteristica v WHERE v.idProveedor = :idProveedor"),
    @NamedQuery(name = "ValorCaracteristica.findByIdComprador", query = "SELECT v FROM ValorCaracteristica v WHERE v.idComprador = :idComprador"),
    @NamedQuery(name = "ValorCaracteristica.findByIdProductoCaracteristicaPadre", query = "SELECT v FROM ValorCaracteristica v WHERE v.idProductoCaracteristicaPadre = :idProductoCaracteristicaPadre"),
    @NamedQuery(name = "ValorCaracteristica.findByIdValorPadre", query = "SELECT v FROM ValorCaracteristica v WHERE v.idValorPadre = :idValorPadre")})
public class ValorCaracteristica implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ValorCaracteristicaPK valorCaracteristicaPK;
    @Basic(optional = false)
    @Column(name = "valor")
    private String valor;
    @Column(name = "id_proveedor")
    private Integer idProveedor;
    @Column(name = "id_comprador")
    private Integer idComprador;
    @Column(name = "id_producto_caracteristica_padre")
    private Integer idProductoCaracteristicaPadre;
    @Column(name = "id_valor_padre")
    private Integer idValorPadre;
    @JoinColumn(name = "id_canal_venta", referencedColumnName = "id")
    @ManyToOne
    private CanalVenta idCanalVenta;
    @JoinColumn(name = "id_metrica", referencedColumnName = "id")
    @ManyToOne
    private Metrica idMetrica;
    @JoinColumn(name = "id_producto_caracteristica", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ProductoCaracteristica productoCaracteristica;

    public ValorCaracteristica() {
    }

    public ValorCaracteristica(ValorCaracteristicaPK valorCaracteristicaPK) {
        this.valorCaracteristicaPK = valorCaracteristicaPK;
    }

    public ValorCaracteristica(ValorCaracteristicaPK valorCaracteristicaPK, String valor) {
        this.valorCaracteristicaPK = valorCaracteristicaPK;
        this.valor = valor;
    }

    public ValorCaracteristica(int id, int idProductoCaracteristica) {
        this.valorCaracteristicaPK = new ValorCaracteristicaPK(id, idProductoCaracteristica);
    }

    public ValorCaracteristicaPK getValorCaracteristicaPK() {
        return valorCaracteristicaPK;
    }

    public void setValorCaracteristicaPK(ValorCaracteristicaPK valorCaracteristicaPK) {
        this.valorCaracteristicaPK = valorCaracteristicaPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    public Integer getIdProductoCaracteristicaPadre() {
        return idProductoCaracteristicaPadre;
    }

    public void setIdProductoCaracteristicaPadre(Integer idProductoCaracteristicaPadre) {
        this.idProductoCaracteristicaPadre = idProductoCaracteristicaPadre;
    }

    public Integer getIdValorPadre() {
        return idValorPadre;
    }

    public void setIdValorPadre(Integer idValorPadre) {
        this.idValorPadre = idValorPadre;
    }

    public CanalVenta getIdCanalVenta() {
        return idCanalVenta;
    }

    public void setIdCanalVenta(CanalVenta idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    public ProductoCaracteristica getProductoCaracteristica() {
        return productoCaracteristica;
    }

    public void setProductoCaracteristica(ProductoCaracteristica productoCaracteristica) {
        this.productoCaracteristica = productoCaracteristica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (valorCaracteristicaPK != null ? valorCaracteristicaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValorCaracteristica)) {
            return false;
        }
        ValorCaracteristica other = (ValorCaracteristica) object;
        if ((this.valorCaracteristicaPK == null && other.valorCaracteristicaPK != null) || (this.valorCaracteristicaPK != null && !this.valorCaracteristicaPK.equals(other.valorCaracteristicaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ValorCaracteristica[ valorCaracteristicaPK=" + valorCaracteristicaPK + " ]";
    }
    
}
