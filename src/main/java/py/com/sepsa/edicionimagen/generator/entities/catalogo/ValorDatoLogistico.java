/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.catalogo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "valor_dato_logistico", catalog = "sepsa", schema = "catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ValorDatoLogistico.findAll", query = "SELECT v FROM ValorDatoLogistico v"),
    @NamedQuery(name = "ValorDatoLogistico.findByValor", query = "SELECT v FROM ValorDatoLogistico v WHERE v.valor = :valor"),
    @NamedQuery(name = "ValorDatoLogistico.findById", query = "SELECT v FROM ValorDatoLogistico v WHERE v.id = :id")})
public class ValorDatoLogistico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "valor")
    private String valor;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumns({
        @JoinColumn(name = "id_caracteristica", referencedColumnName = "id"),
        @JoinColumn(name = "id_tipo_caracteristica", referencedColumnName = "id_tipo_caracteristica")})
    @ManyToOne(optional = false)
    private Caracteristica caracteristica;
    @JoinColumn(name = "id_dato_logistico", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DatoLogistico idDatoLogistico;
    @JoinColumn(name = "id_metrica", referencedColumnName = "id")
    @ManyToOne
    private Metrica idMetrica;

    public ValorDatoLogistico() {
    }

    public ValorDatoLogistico(Integer id) {
        this.id = id;
    }

    public ValorDatoLogistico(Integer id, String valor) {
        this.id = id;
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Caracteristica getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(Caracteristica caracteristica) {
        this.caracteristica = caracteristica;
    }

    public DatoLogistico getIdDatoLogistico() {
        return idDatoLogistico;
    }

    public void setIdDatoLogistico(DatoLogistico idDatoLogistico) {
        this.idDatoLogistico = idDatoLogistico;
    }

    public Metrica getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Metrica idMetrica) {
        this.idMetrica = idMetrica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValorDatoLogistico)) {
            return false;
        }
        ValorDatoLogistico other = (ValorDatoLogistico) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.catalogo.ValorDatoLogistico[ id=" + id + " ]";
    }
    
}
