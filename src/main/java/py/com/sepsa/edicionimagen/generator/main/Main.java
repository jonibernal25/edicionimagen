/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.main;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import py.com.sepsa.edicionimagen.generator.pojos.Resultado;
import py.com.sepsa.edicionimagen.generator.utils.BdUtils;
import py.com.sepsa.edicionimagen.generator.utils.JpaUtil;
import py.com.sepsa.edicionimagen.generator.utils.LogHandler;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class Main {

    /**
     * Ruta al archivo de configuración del sistema
     */
    private static String config = "config.json";
    //private static String config = "C:\\Users\\Sepsa\\Desktop\\EdicionImagen\\config.json";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            File fileConfig = new File(config);

            if (!fileConfig.exists()) {
                System.out.println("No existe el archivo de configuración");
                System.exit(1);
            }

            String configData = IOUtils.toString(new FileInputStream(fileConfig));

            JSONObject json = new JSONObject(configData);
            JpaUtil.setURL_SEPSA(json.getString("urlSepsa"));
            JpaUtil.setDB_USER_SEPSA(json.getString("userSepsa"));
            JpaUtil.setDB_PASS_SEPSA(json.getString("passwordSepsa"));
            String mailReporte = json.getString("mailResporte");
            Long sleepTime = json.getLong("sleepTime");
            String path = json.getString("path");
            String user = json.getString("user");
            
            File filePath = new File(path);
            
            if (!filePath.exists()) {
                System.out.println("No existe existe la ruta");
                System.exit(1);
            }
            
            while (true) {                
                BdUtils bdUtils = new BdUtils();
                Upload upload = new Upload(bdUtils);

                /*String id = GoogleDriveUtils.findFileId("Edicion", "mimeType = 'application/vnd.google-apps.folder' and trashed = false");

                if(id == null) {
                    GoogleDriveUtils.createFolder("Edicion", null);
                }*/

                String id = "1zczZqIpJWjPgeV3HqR6_3zsAanY5fcWA";

                String sinProducoFolder = String.format("%s/erroneas/sin_producto", path);
                File sinProducto = new File(sinProducoFolder);
                if (!sinProducto.exists()) {
                    sinProducto.mkdirs();
                }

                String otrosFolder = String.format("%s/erroneas/otros", path);
                File otros = new File(otrosFolder);
                if (!otros.exists()) {
                    otros.mkdirs();
                }

                String repetidosFolder = String.format("%s/erroneas/repetidos", path);
                File repetidos = new File(repetidosFolder);
                if (!repetidos.exists()) {
                    repetidos.mkdirs();
                }

                String procesadosFolder = String.format("%s/procesados", path);
                File procesados = new File(procesadosFolder);
                if (!procesados.exists()) {
                    procesados.mkdirs();
                }
                List<Resultado> resultado = new ArrayList<>();
                boolean ok = true;

                while (ok) {
                    try {

                        filePath = new File(path);

                        for (File file : filePath.listFiles()) {
                            if((file.isDirectory()
                                    && !file.getName().equals("erroneas")
                                    && !file.getName().equals("procesados"))) {
                                upload.process(file, id, user, sinProducoFolder, otrosFolder, procesadosFolder, repetidosFolder, resultado);
                            }
                        }
                        ok = false;
                    } catch(Throwable ex) {
                        ok = true;
                        LogHandler.logFatal("Se ha producido el siguietn error:", ex);
                    }
                }

                if(!resultado.isEmpty()) {
                    ReportGenerator.generateReporte(resultado, mailReporte);
                }

                try {
                    Thread.sleep(sleepTime);
                } catch(Exception ex) {
                    LogHandler.logFatal("Se ha producido el siguietn error:", ex);
                }
            }
        } catch(Exception ex) {
            LogHandler.logFatal("Se ha producido un error:", ex);
        }
    }
    
}
