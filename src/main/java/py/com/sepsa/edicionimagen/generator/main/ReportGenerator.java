/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.main;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;
import javax.activation.DataHandler;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.com.sepsa.edicionimagen.generator.pojos.Resultado;
import py.com.sepsa.edicionimagen.generator.utils.MailUtils;
import py.com.sepsa.edicionimagen.generator.utils.Report;

/**
 * Clase para la generación de reporte
 * @author Jonathan D. Bernal Fernández
 */
public class ReportGenerator {
    
    
    public static void generateReporte(List<Resultado> resultados,
            String mails) throws IOException, MessagingException {
        
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Resultado");


        XSSFFont headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 10);

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        CellStyle numberCellStyle = workbook.createCellStyle();
        numberCellStyle.setFont(headerFont);
        numberCellStyle.setDataFormat((short) 3);

        XSSFFont resaltFont = workbook.createFont();
        resaltFont.setBold(true);
        resaltFont.setFontHeightInPoints((short) 10);

        CellStyle resaltCellStyle = workbook.createCellStyle();
        resaltCellStyle.setFont(resaltFont);

        CellStyle resaltCenterCellStyle = workbook.createCellStyle();
        resaltCenterCellStyle.setAlignment(HorizontalAlignment.CENTER);
        resaltCenterCellStyle.setFont(resaltFont);

        XSSFRow dateRow = sheet.createRow(1);

        XSSFCell cell1 = dateRow.createCell(1);
        cell1.setCellValue("Fecha Proceso:");
        cell1.setCellStyle(resaltCellStyle);

        cell1 = dateRow.createCell(2);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cell1.setCellValue(sdf.format(Calendar.getInstance().getTime()));
        cell1.setCellStyle(resaltCenterCellStyle);
        
        XSSFRow headerRow = sheet.createRow(3);

        XSSFCell cell = headerRow.createCell(1);
        cell.setCellValue("Ruc");
        cell.setCellStyle(resaltCellStyle);

        cell = headerRow.createCell(2);
        cell.setCellValue("Gtin");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(3);
        cell.setCellValue("Tipo");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(4);
        cell.setCellValue("Observacion");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(5);
        cell.setCellValue("Nombre Archivo");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(6);
        cell.setCellValue("Hash");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(7);
        cell.setCellValue("Archivo Duplicado");
        cell.setCellStyle(resaltCenterCellStyle);

        int rowCount = 4;
        
        for (int i = 0; i < resultados.size(); i++) {
            XSSFRow row = sheet.createRow(rowCount + i);

            cell = row.createCell(1);
            cell.setCellValue(resultados.get(i).getRuc());
            cell.setCellStyle(resaltCellStyle);

            cell = row.createCell(2);
            cell.setCellValue(resultados.get(i).getCodigoGtin());
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(3);
            cell.setCellValue(resultados.get(i).getTipo().name());
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(4);
            cell.setCellValue(resultados.get(i).getObservacion());
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(5);
            cell.setCellValue(resultados.get(i).getNombreArchivo());
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(6);
            cell.setCellValue(resultados.get(i).getHash());
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(7);
            cell.setCellValue(resultados.get(i).getArchivoDuplicado());
            cell.setCellStyle(headerCellStyle);
        }
        
        ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
        workbook.write(fileOut);

        List<String> list = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(mails, ";");
        while (st.hasMoreElements()) {
            String nextElement = st.nextToken();
            list.add(nextElement);
        }
        
        sendMaild(fileOut.toByteArray(), list);
    }
    
    public static void sendMaild(byte[] bytes, List<String> mails) throws MessagingException {
        MimeMultipart mp = new MimeMultipart("related");
            
        MimeBodyPart att = new MimeBodyPart(); 
        ByteArrayDataSource bds = new ByteArrayDataSource(bytes, MailUtils.MimeType.XLS.getValue()); 
        att.setDataHandler(new DataHandler(bds)); 
        att.setFileName("Reporte proceso imagen"); 
        mp.addBodyPart(att);

        Report t = new Report();
        t.setName("Reporte de subida de imágenes");
        t.setMailToList(mails);
        List<Report> tos = new ArrayList();
        tos.add(t);
        MailUtils.send(tos, mp, "Reporte de Procesamiento de Imágenes");
    }
}
