/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.main;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;
import org.json.JSONObject;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.EdicionImagen;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.HistoricoEdicionImagen;
import py.com.sepsa.edicionimagen.generator.utils.BdUtils;
import py.com.sepsa.edicionimagen.generator.utils.FileUtils;
import py.com.sepsa.edicionimagen.generator.utils.GoogleDriveUtils;
import py.com.sepsa.edicionimagen.generator.utils.LogHandler;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.Etiqueta;
import py.com.sepsa.edicionimagen.generator.entities.info.Producto;
import py.com.sepsa.edicionimagen.generator.entities.trans.Usuario;
import py.com.sepsa.edicionimagen.generator.pojos.Resultado;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class Upload {

    public Upload(BdUtils bdUtils) {
        this.bdUtils = bdUtils;
    }
    
    /**
     * Manejador de base de datos
     */
    private BdUtils bdUtils;
    
    /**
     * Procesa la subida de un archivo a google drive y a la bd
     * @param file 
     * @param parentId
     * @param usuario
     * @param sinProductoFolder
     * @param otrosFolder
     * @param procesadosFolder
     * @param repetidoFolder
     * @param result Resultado
     * @throws Exception 
     */
    public void process(File file, String parentId, String usuario,
            String sinProductoFolder, String otrosFolder,
            String procesadosFolder, String repetidoFolder, List<Resultado> result) throws Throwable {
        
        Etiqueta etiqueta = bdUtils.findEtiqueta("CARGADO");
        
        if(etiqueta == null) {
            LogHandler.logError("No se encuentra la etiqueta: CARGADO");
            return;
        }
        
        if(!file.isDirectory()) {
            LogHandler.logError(String.format("%s no es una carpeta", file.getName()));
            return;
        }
        
        String ruc = file.getName();
        
        String q = String.format("mimeType = 'application/vnd.google-apps.folder' and trashed = false and name = '%s' and '%s' in parents", ruc, parentId);
        String rucFolderId = GoogleDriveUtils.findFileId(ruc, q);
        if(rucFolderId == null) {
            rucFolderId = GoogleDriveUtils.createFolder(ruc, parentId);
        }
        
        File[] fileList = file.listFiles();
        
        for (File imageFile : fileList) {
            
            if(!imageFile.isFile()) {
                LogHandler.logError(String.format("No es un archivo: %s", imageFile.getName()));
                String path = String.format("%s/%s", otrosFolder, file.getName());
                File temp = new File(path);
                if(!temp.exists()) {
                    temp.mkdirs();
                }
                FileUtils.copy(imageFile, String.format("%s/%s", path, imageFile.getName()));
                imageFile.delete();
                continue;
            }

            String fileName = imageFile.getName();
            String codigoGtin = "";
            StringTokenizer st = new StringTokenizer(fileName, "_");
            if(st.countTokens() > 0) {
                codigoGtin = st.nextToken();
            }

            String hash = hashSha256(imageFile);
            
            List<Producto> productos = bdUtils.findProducto(codigoGtin, ruc);
            
            if(productos == null || productos.isEmpty()) {
                String mensaje = String.format("Archivo sin producto/proveedor registrado en recepción de muestra: %s", imageFile.getName());
                LogHandler.logError(mensaje);
                Resultado resultado = new Resultado(ruc, codigoGtin, imageFile.getName(), null, hash, Resultado.Type.SIN_PRODUCTO, mensaje);
                result.add(resultado);
                String path = String.format("%s/%s", sinProductoFolder, file.getName());
                File temp = new File(path);
                if(!temp.exists()) {
                    temp.mkdirs();
                }
                FileUtils.copy(imageFile, String.format("%s/%s", path, imageFile.getName()));
                imageFile.delete();
                continue;
            }
            
            HistoricoEdicionImagen hei = bdUtils.findHistoricoEdicionImagen(hash);
            if(hei != null) {
                String mensaje = String.format("Archivo repetido: %s; %s; %s", imageFile.getName(), hei.getNombreArchivo(), hash);
                LogHandler.logError(mensaje);
                Resultado resultado = new Resultado(ruc, codigoGtin, hei.getNombreArchivo(), imageFile.getName(), hash, Resultado.Type.REPETIDO, mensaje);
                result.add(resultado);
                String path = String.format("%s/%s", repetidoFolder, file.getName());
                File temp = new File(path);
                if(!temp.exists()) {
                    temp.mkdirs();
                }
                FileUtils.copy(imageFile, String.format("%s/%s", path, imageFile.getName()));
                imageFile.delete();
                continue;
            }
            
            for (Producto producto : productos) {
                
                String id = GoogleDriveUtils.uploadImage(imageFile, rucFolderId);
                q = String.format("trashed = false and '%s' in parents and name = '%s'", rucFolderId, fileName);
                com.google.api.services.drive.model.File gdFile = GoogleDriveUtils.findFile(id, q);

                EdicionImagen edicionImagen = new EdicionImagen();
                edicionImagen.setIdProducto(producto.getId());
                bdUtils.create(edicionImagen);

                Usuario user = bdUtils.findUsuario(usuario);

                HistoricoEdicionImagen item = 
                        new HistoricoEdicionImagen();

                item.setIdEtiqueta(etiqueta);
                item.setIdEdicionImagen(edicionImagen);
                item.setIdUsuario(user);
                item.setFecha(Calendar.getInstance().getTime());
                item.setHash(hash);
                item.setNombreArchivo(fileName);
                item.setDispositivo(getImageDetails(imageFile));
                item.setIdUsuarioEditor(null);
                item.setUrl(gdFile.getWebViewLink());
                item.setDownloadUrl(gdFile.getWebContentLink());

                bdUtils.create(item);

                edicionImagen.setIdHistoricoEdicionImagen(item);
                bdUtils.edit(edicionImagen);
            }
            
            Resultado resultado = new Resultado(ruc, codigoGtin, imageFile.getName(), null, hash, Resultado.Type.CARGADO, "");
            result.add(resultado);
            
            String path = String.format("%s/%s", procesadosFolder, file.getName());
            File temp = new File(path);
            if(!temp.exists()) {
                temp.mkdirs();
            }
            FileUtils.copy(imageFile, String.format("%s/%s", path, imageFile.getName()));
            imageFile.delete();
        }
        
        file.delete();
    }
    
    private String getImageDetails(File file) {
        
        JSONObject json = new JSONObject();
        try(InputStream is = new FileInputStream(file)) {
            Metadata metadata = ImageMetadataReader.readMetadata(is);

            for (Directory directory : metadata.getDirectories()) {
                for (Tag tag : directory.getTags()) {
                    
                    switch(tag.getTagName()) {
                        case "Make":
                        case "Model":
                        case "Image Width":
                        case "Image Height":
                            json.put(tag.getTagName(), tag.getDescription());
                            break;
                    }
                }
            }
        } catch(Exception ex) {
        }
        
        return json.toString();
    }
    
    private static final String hashSha256(File file) {
        String result = "N/A";
        
        try(InputStream stream = new FileInputStream(file)) {
            final byte[] buffer = new byte[1024 * 1024];
            final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            int bytesRead = 0;
            while ((bytesRead = stream.read(buffer)) >= 0) {
                if (bytesRead > 0) {
                    sha256.update(buffer, 0, bytesRead);
                }
            }
            byte[] digest = sha256.digest();
            
            result = String.format("%064x", new BigInteger(1, digest));
        } catch (Exception e) {
        }
        
        return result;
    }
}
