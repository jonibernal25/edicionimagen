/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.utils;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.DetalleRecepcionMuestra;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.EstadoCarga;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.Etiqueta;
import py.com.sepsa.edicionimagen.generator.entities.catalogo.HistoricoEdicionImagen;
import py.com.sepsa.edicionimagen.generator.entities.info.Local;
import py.com.sepsa.edicionimagen.generator.entities.info.Persona;
import py.com.sepsa.edicionimagen.generator.entities.info.Producto;
import py.com.sepsa.edicionimagen.generator.entities.trans.Usuario;


/**
 *
 * @author Rubén Ortiz
 */
public class BdUtils {

    /**
     * Fábrica del manejador de entidades
     */
    private EntityManagerFactory emfSepsa = null;
    
    
    /**
     * Obtiene la descripcion de un local
     * @param idLocal Identificador de local
     * @param idPersona Identificador de persona
     * @return Descripcion de local
     */
    public Local findLocal(Integer idLocal, Integer idPersona) {
        
        Local result = null;
        
        if(idLocal != null && idPersona != null) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
            Root<Local> root = cq.from(Local.class);
            cq.select(root);

            cq.where(qb.and(qb.equal(root.get("localPK").get("id"), idLocal),
                    qb.equal(root.get("localPK").get("idPersona"), idPersona)));

            javax.persistence.Query q = getEntityManagerSepsa()
                    .createQuery(cq)
                    .setHint("eclipselink.refresh", "true");

            List<Local> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
            
        return result;
    }

    private EntityManager getEntityManagerSepsa() {
        if(emfSepsa == null) {
            emfSepsa = JpaUtil.getEntityManagerFactorySepsa();
        }
        return emfSepsa.createEntityManager();
    }
    
    /*
    * Constructor
     */
    public BdUtils() {}
    
    /**
     * Obtiene el siguiente valor de la secuencia para el identificador de la entidad documento
     * @return Siguiente valor de la secuencia
     */
    public Integer triggerSequence() {
        javax.persistence.Query q = getEntityManagerSepsa().createNativeQuery("select nextval('trans.documento_id_seq')");
        Long nextVal = (Long) q.getSingleResult();
        return nextVal.intValue();
    }
    
    /**
     * Obtiene una etiqueta segun descripción
     * @param descripcion Descripción
     * @return Etiqueta
     */
    public Etiqueta findEtiqueta(String descripcion) {
        Etiqueta result = null;
        if(descripcion != null && !descripcion.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Etiqueta> root = cq.from(Etiqueta.class);
            cq.select(root);
            cq.where(qb.equal(root.get("descripcion"), descripcion));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            List<Etiqueta> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
        return result;
    }
    
    /**
     * Obtiene un historico edicion imagen segun filtro
     * @param hash Hash
     * @return HistoricoEdicionImagen
     */
    public HistoricoEdicionImagen findHistoricoEdicionImagen(String hash) {
        HistoricoEdicionImagen result = null;
        if(hash != null && !hash.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<HistoricoEdicionImagen> root = cq.from(HistoricoEdicionImagen.class);
            cq.select(root);
            cq.where(qb.equal(root.get("hash"), hash));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            List<HistoricoEdicionImagen> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
        return result;
    }
    
    /**
     * Obtiene una lista de productos de recepción de muestra
     * @param codigoGtin Código GTIN
     * @param ruc Ruc
     * @return Etiqueta
     */
    public List<Producto> findProducto(String codigoGtin, String ruc) {
        List<Producto> result = new ArrayList<>();
        if(codigoGtin != null && !codigoGtin.trim().isEmpty()
                && ruc != null && !ruc.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Producto> root = cq.from(Producto.class);
            Root<DetalleRecepcionMuestra> drc = cq.from(DetalleRecepcionMuestra.class);
            Join<Producto, Persona> p = root.join("idPersona");
            
            cq.select(root).distinct(true);
            cq.where(qb.and(
                    qb.equal(root.get("id"), drc.get("detalleRecepcionMuestraPK").get("idProducto")),
                    qb.equal(root.get("codigoGtin"), codigoGtin),
                    qb.equal(p.get("ruc"), ruc)));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            result = q.getResultList();
        }
        return result;
    }
    
    /**
     * Obtiene un usuario
     * @param usuario Usuario
     * @return Usuario
     */
    public Usuario findUsuario(String usuario) {
        Usuario result = null;
        if(usuario != null && !usuario.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Usuario> root = cq.from(Usuario.class);
            cq.select(root);
            cq.where(qb.equal(root.get("usuario"), usuario));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            List<Usuario> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
        return result;
    }
    
    /**
     * Edita una instancia de documento
     * @param object Objeto
     * @return 
     */
    public <T> boolean edit(T object) {
        boolean saved = true;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            em.getTransaction().begin();
            em.merge(object);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            saved = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return saved;
    }
    
    /**
     * Edita una instancia de documento
     * @param object Objeto
     * @return 
     */
    public <T> boolean create(T object) {
        boolean saved = true;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            saved = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return saved;
    }
    
    /**
     * Edita una instancia de documento
     * @param documento Documento
     * @return 
     */
    public Object find(Class clazz, Object id) {
        Object t = null;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            t = em.find(clazz, id);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return t;
    }
}
